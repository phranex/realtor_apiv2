function isEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
}

function isUrl(string) {
    return string.indexOf('http') >= 0 && string.indexOf('.com') >= 0;
}

function isAlphaNumeric(string) {
    var re = /^[a-zA-Z0-9]+$/i;
    return re.test(string);
}

function isNumeric(string) {
    var re = /^[0-9]+$/i;
    return re.test(string);
}

function isAlpha(string) {
    var re = /^[a-zA-Z ]+$/i;
    return re.test(string);
}

function isNotEmpty(field) {
    var empty = true;
    $(field).each(function(i, obj) {
        if (!$(this).val().length > 0) {

            return empty = false
        }
    });
    console.log(empty);
    return empty;

}

function validate(data, errorClass) {
    var items = Object.keys(data).length;
    var flag = 0;
    for (const key in data) {
        if (data.hasOwnProperty(key)) {
            const values = data[key];
            var rules = values.split('|');
            var message = [];
            for (i = 0; i < rules.length; i++) {
                console.log(key + ' ' + rules[i] + ' ' + rules.length);
                mess = validateRules(key, rules[i], errorClass);
                if (mess.length > 0 && mess != '')
                    message.push(mess);

            }
            if (message.length > 0) {
                flag++;
                displayErrorMessages(key, message);

            }

        }
    }


    return !flag;
}

function displayErrorMessages(ele, messages) {
    $(ele).siblings('.validation-holder').remove();
    var mess = "<div class='validation-holder'><span class='validation-message'>";
    for (i = 0; i < messages.length; i++) {
        if (messages[i].length > 0)
            mess += messages[i] + ', ';
    }
    mess += '</span></div>';
    $(ele).after(mess);
}

function validateRules(key, rule, errorClass) {
    $(key).siblings('.validation-holder').remove();
    var message = '';
    errors = 0;
    if (rule.indexOf("=") >= 0) {
        var r = rule.split('=');
        console.log(r);
        if (r[0] == 'max') {
            if (!isEmpty(key)) {
                console.log(r[1] + ' max ' + $(key).val().length);
                var max = r[1];
                var length = $(key).val().length;
                if (length > max) {
                    $(key).addClass(errorClass);
                    errors++;
                    message += 'Maximum characters allowed is ' + r[1] + ". There are " + length + " characters in the box.";
                }
            }
        }
        if (r[0] == 'min') {
            if (!isEmpty(key)) {
                console.log(r[1] + ' min ' + $(key).val().length);
                var min = r[1];
                var length = $(key).val().length;
                if (length < min) {
                    $(key).addClass(errorClass);
                    errors++;
                    message += 'Minimum characters allowed is ' + r[1] + ". There are " + length + " characters in the box.";
                }
            }
        }
    }
    if (rule == 'required') {
        if (isEmpty(key)) {
            $(key).addClass(errorClass);
            errors++;
            message += 'This field is required';
        } else {
            $(key).removeClass(errorClass);
        }
    }
    if (rule == 'alpha') {
        if (!isEmpty(key)) {
            if (!isAlpha($(key).val())) {
                message += 'Only Letters are allowed';
                $(key).addClass(errorClass);
                errors++;
            } else {
                $(key).removeClass(errorClass);
                $(key).siblings('.validation-holder').remove();
            }
        }
    }
    if (rule == 'number') {
        if (!isEmpty(key)) {
            if (!isNumeric($(key).val())) {
                message += 'Only numbers are allowed';
                $(key).addClass(errorClass);
                errors++;
            } else {
                $(key).removeClass(errorClass);
                $(key).siblings('.validation-holder').remove();
            }
        } else
            $(key).removeClass(errorClass);
    }

    if (rule == 'url') {
        if (!isEmpty(key)) {
            if (!isUrl($(key).val())) {
                message += 'Enter a valid url. Must contain (http) and (.com)';
                $(key).addClass(errorClass);
                errors++;
            } else {
                $(key).removeClass(errorClass);
                $(key).siblings('.validation-holder').remove();
            }
        } else
            $(key).removeClass(errorClass);
    }
    // $(key).after("<div class='validation-holder'><span class='validation-message'>" + message + "</span><br/></div>");
    console.log(message);
    return message;
}

function isEmpty(ele) {
    if ($(ele).val() != 0) {
        if ($(ele).val().length > 0 || $(ele).val() != '') return false;

    }
    return true;

}
