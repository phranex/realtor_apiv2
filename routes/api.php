<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//auth
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('register/{social?}', 'AuthController@register');
    Route::post('forgot-password','Auth\ForgotPasswordController@sendResetLinkEmail' );
    Route::post('change-password','AuthController@changePassword' );
    Route::post('reset','Auth\ResetPasswordController@reset' );
    Route::post('me', 'AuthController@me');

});


//user profile settings

Route::group(['middleware' => 'jwt','prefix' => 'user'], function () {
    Route::post('personal-settings/store','PersonalSettingController@store');
    Route::post('upload-avatar','PersonalSettingController@uploadAvatar');

    Route::get('/dashboard/get','Api\UserController@index');
    Route::get('/stripe', function(){

        $user = auth()->user();
        // $x = $user->createAsStripeCustomer();
        $y = $user->createSetupIntent();
       return apiResponse(1,'s', $y);
        return view('stripe');
    });

    Route::post('/payment-method/store', 'Api\PaymentController@store');
});

Route::group(['prefix' => 'user/contact','middleware' => 'jwt'], function () {
    Route::post('/store', 'Api\ContactController@store');
    Route::get('/index', 'Api\ContactController@index');
    Route::post('/update', 'Api\ContactController@update');
    Route::get('/search/get', 'Api\ContactController@search');
    Route::get('/filter/get', 'Api\ContactController@filter');
    Route::post('/delete/{id}', 'Api\ContactController@destroy');
    Route::get('/get-bulk-format/','Api\ContactController@getBulkFormat');

});

Route::group(['prefix' => 'user/templates','middleware' => 'jwt'], function () {

    Route::get('/', 'Api\TemplateController@index');


});



//social
Route::group(['middleware' => 'jwt','prefix' => 'social'], function () {
    //social links allowed
    Route::get('get-types', 'Api\SocialLinkTypeController@getTypes');
    Route::post('store', 'Api\SocialLinkController@store');
    Route::get('delete', 'Api\SocialLinkController@delete');
    Route::post('update', 'Api\SocialLinkController@update');
});


// invitation

Route::group(['prefix' => 'user/invitation', 'middleware' => 'jwt'], function () {
    Route::get('/', 'Api\InvitationController@index');
    Route::post('store', 'Api\InvitationController@store');


});
//without jwt

Route::get('/packages/get', 'Api\PackageController@index');
Route::get('/packages/get/{id}', 'Api\PackageController@show');

Route::group(['prefix' => '/'], function () {
    //view invitation form
    Route::get('user/invitation/showByCode/{code}', 'Api\InvitationController@showByCode');
    //review
    // Route::post('user/review/store/{anonymous?}', 'Api\ReviewController@store');
    Route::post('review/store/save', 'Api\ReviewController@store');
    Route::get('reviews', 'Api\ReviewController@index');

    //get realtor profile
    Route::get('user/{business_name}/profile', 'Api\UserController@show');





});
 //get reviews via js

 Route::group(['middleware' => 'cors', 'prefix' => ''], function () {
    // header('Access-Control-Allow-Origin: *');
    // header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
    // header('Access-Control-Allow-Headers: Origin, Accept, Content-Type, X-Auth-Token');
    Route::get('reviews/integrations/get', 'Api\ReviewController@getRealtorReviews');

 });

Route::group(['prefix' => 'search'], function(){
    Route::get('/realtor', 'Api\UserController@searchByBusinessName');
});


//review



//replies
Route::group(['prefix' => 'reply','middleware' => 'jwt'], function(){
    Route::post('/store/{id}', 'Api\ReplyController@store');
});


//integrations
Route::group(['prefix' => 'integrations','middleware' => 'jwt'], function(){
    Route::get('/types', 'Api\IntegrationController@getTypes');
    // Route::post('store','Api\IntegrationController@store');
});



Route::group(['prefix' => 'user/integrations', 'middleware' => 'jwt'], function () {
    Route::get('/',  'Api\IntegrationController@index');
    Route::post('store', 'Api\IntegrationController@store');


});

//subscription

Route::post('/subscription/store', 'Api\SubscriptionController@store');




//page setup

Route::get('/page-setup/{page}', 'SettingController@pageSetup');

