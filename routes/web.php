<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
    // return view('welcome');
});


Route::get('/index', 'DashboardController@index')->name('admin.dashboard');


//social links
Route::get('/social-link/create', 'SocialLinkTypeController@create')->name('social.link.create');
Route::post('/social-link/store', 'SocialLinkTypeController@store')->name('social.link.store');

//integrations
Route::group(['prefix' => 'integration'], function () {
    Route::get('create','ThirdPartyIntegrationController@create')->name('integration.create');
    Route::post('store','ThirdPartyIntegrationController@store')->name('integration.store');
});


Route::group(['prefix' => 'admin'], function () {
    Route::get('/settings', 'SettingController@index')->name('admin.settings');



    //users

    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'UserController@index')->name('admin.users');
        Route::get('/{id}', 'UserController@show')->name('admin.user.show');
    });


    //packages

    Route::group(['prefix' => 'features'], function () {
        Route::get('/', 'FeaturesController@index')->name('admin.features');
        Route::get('/create', 'FeaturesController@create')->name('admin.features.create');
        Route::post('/store', 'FeaturesController@store')->name('admin.features.store');
        Route::get('/edit/{id}', 'FeaturesController@edit')->name('admin.features.edit');
        Route::post('/update/{id}', 'FeaturesController@update')->name('admin.features.update');
        Route::get('/delete/{id}', 'FeaturesController@destroy')->name('admin.features.delete');
    });


    Route::group(['prefix' => 'packages'], function () {
        Route::get('/', 'PackageController@index')->name('admin.packages');
        Route::get('/create', 'PackageController@create')->name('admin.package.create');
        Route::post('/store', 'PackageController@store')->name('admin.package.store');
        Route::get('/edit/{id}', 'PackageController@edit')->name('admin.package.edit');
        Route::post('/update/{id}', 'PackageController@update')->name('admin.package.update');
        Route::get('/delete/{id}', 'PackageController@destroy')->name('admin.package.delete');
    });

    Route::post('/site/home', 'SettingController@indexPage')->name('admin.site.index');
    Route::post('/site/how-it-works', 'SettingController@how_it_works_page')->name('admin.site.hiw');
    Route::post('/site/how-it-works', 'SettingController@hiw_page')->name('admin.site.hiw2');

    Route::post('/site/contact', 'SettingController@contact_page')->name('admin.site.contact');
    Route::post('/site/faq', 'SettingController@faq_page')->name('admin.site.faq');
    Route::post('/site/features', 'SettingController@features')->name('admin.site.features');
    Route::post('/site/instruction', 'SettingController@instruction')->name('admin.site.instruction');
    Route::post('/site/terms', 'SettingController@terms')->name('admin.site.terms');
    Route::post('/site/payments', 'SettingController@payments')->name('admin.site.payments');
    Route::post('/site/guarantee', 'SettingController@guarantee')->name('admin.site.guarantee');
    Route::post('/site/personal', 'SettingController@personal')->name('admin.site.personal');
    Route::post('/site/upload', 'SettingController@uploadBanner')->name('admin.site.banner');
    Route::get('/site/upload/delete-banner/{banner?}', 'SettingController@deleteBanner')->name('admin.site.banner.delete');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::group(['prefix' => 'subscription'], function () {
    Route::get('/', 'SubscriptionController@index')->name('admin.subscriptions');

});


