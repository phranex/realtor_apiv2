@extends('layouts.api.email.master')

@section('content')
{{-- Hi there, thank you for being a valued Secure bot customer. Your opinion is very important to us and would like to get your feedback on how we're doing.

The information you provide will help us to improve our customer experience for you and the entire Secure bot community. --}}
{!! $mess !!}


@endsection

@push('action')
    {{$url}}
@endpush
