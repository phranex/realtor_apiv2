<aside class="app-navbar">
    <!-- begin sidebar-nav -->
    <div class="sidebar-nav scrollbar scroll_light">
        <ul class="metismenu " id="sidebarNav">
            <li class="nav-static-title">Personal</li>
            <li class="@if(request()->route()->getName() ==  'user.dashboard') active @endif">
                <a class="" href="{{route('admin.dashboard')}}" aria-expanded="false">
                    <i class="nav-icon ti ti-rocket"></i>
                    <span class="nav-title">Dashboard</span>

                </a>

            </li>
        <li class="@if(request()->route()->getName() ==  'user.users') active @endif"><a href="{{route('admin.users')}}" aria-expanded="false"><i class="nav-icon ti ti-comment"></i><span class="nav-title">Realtors</span></a> </li>
        <li class="@if(request()->route()->getName() ==  'user.users') active @endif">
            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-key"></i><span class="nav-title">Packages</span></a>
            <ul aria-expanded="false" class="collapse" style="">
                <li> <a href="{{route('admin.features')}}">Features</a> </li>
                <li> <a href="{{route('admin.packages')}}">Packages</a> </li>

            </ul>
        </li>
            </li>
            <li class="@if(request()->route()->getName() ==  'user.sunscriptions') active @endif">
            <a class="has-arro" href="{{route('admin.subscriptions')}}" aria-expanded="false"><i class="nav-icon ti ti-info"></i><span class="nav-title">Subscriptions</span> </a>

            </li>
            <li class="@if(request()->route()->getName() ==  'user.reviews') active @endif"><a href="" aria-expanded="false"><i class="nav-icon ti ti-email"></i><span class="nav-title">Application Settings</span></a> </li>
            <li class="@if(request()->route()->getName() ==  'user.integrations') active @endif">
            <a class="has-arro" href="{{route('admin.settings')}}" aria-expanded="false"><i class="nav-icon ti ti-bag"></i> <span class="nav-title">Site Settings</span></a>

            </li>
            <li class="@if(request()->route()->getName() ==  'user.dashboard-profile') active @endif">
                <a class="has-arrow" href="" aria-expanded="false"><i class="nav-icon ti ti-info"></i><span class="nav-title">Profile</span> </a>

            </li>


            {{--<li class="nav-static-title">Widgets, Tables & Layouts</li>--}}

            {{--<li class="@if(request()->route()->getName() ==  'user.reviews') active @endif">--}}
                {{--<a class="has-arrow" href="{{route('user.reviews')}}" aria-expanded="false"><i class="nav-icon ti ti-layout-column3-alt"></i><span class="nav-title">Settings</span></a>--}}

            {{--</li>--}}

            {{--<li class="@if(request()->route()->getName() ==  'user.reviews') active @endif">--}}
                {{--<a class="has-arrow" href="{{route('user.reviews')}}" aria-expanded="false"><i class="nav-icon ti ti-layout-column3-alt"></i><span class="nav-title">Support</span></a>--}}

            {{--</li>--}}





        </ul>
    </div>
    <!-- end sidebar-nav -->
</aside>
