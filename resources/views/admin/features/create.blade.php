@extends('layouts.admin')


@section('content')
<div class="col-12 ">
    <div class="card card-statistics">
        <div class="card-header">
            <div class="card-heading">
                <h4 class="card-title">{{config('app.name')}} Features

                </h4>
            </div>
        </div>
        <div class="card-body">
            <form method='post' action="{{route('admin.features.store')}}" class="form-horizontal mt-4">
                @csrf
                <div class="form-group">
                    <label>Name <span class="help"></span></label>
                <input required name='name' required type="text" class="form-control" value="@if(old('name'))  {{old('name')}}  @endif">
                </div>
                <div class="form-group">
                        <label>Feature Type </label>
                        <small><span class="help">Please select the type of values this feature can take</span></small>
                        <select style="width:30%" class="form-control" name="type">
                            <option value="1">Numbers</option>
                            <option value="2">Conditional</option>
                        </select>
                </div>
                <div class="form-group">
                    <label>Description <span class="help"></span></label>
                    <textarea required required name="description"  class="form-control" rows="5">@if(old('description'))  {{old('description')}}  @endif</textarea>
                </div>

                <div class="form-group m-t-15">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
