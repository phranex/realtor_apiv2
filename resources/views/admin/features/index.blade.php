@extends('layouts.admin')


@section('content')
<div class="col-12 ">
    <div class="card card-statistics">
        <div class="card-header">
            <div class="card-heading">
                <h4 class="card-title">{{config('app.name')}} Features
                {{-- <Small class="text-bold">Total: {{count($users)}}</Small> --}}
                </h4>
                <div class="text-right">
                    <a class="btn btn-sm btn-primary" href="{{route('admin.features.create')}}">Add Feature</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table mb-0">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Description</th>
                            <th scope="col">Status</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @isset($features)
                            @if(count($features))
                                @foreach($features as $feature)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$feature->name}} </td>
                                        <td>{{$feature->description}}</td>
                                        <td>
                                            @if($feature->status)
                                                <small style="border:1px solid red;border-radius:12px;padding:2px"><i class="fa fa-check text-success"></i> Active</small>
                                            @else
                                            <small style="border:1px solid red;border-radius:12px;padding:2px 5px"><i  class="fa fa-times text-danger"></i> Not  Active</small>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{route('admin.features.edit',$feature->id)}}" class="btn btn-xs btn-outline-primary"> <i class="fa fa-edit"></i> Edit</a>
                                        <a href="{{route('admin.features.delete',$feature->id)}}" class="btn delete btn-xs btn-outline-danger"> <i class="fa fa-trash"></i> Delete</a>
                                        </td>

                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6">
                                        <div class="alert text-center ">No Feature created</div>
                                    </td>
                                </tr>
                            @endif

                        @endisset
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')

    <script>
        $(document).ready(function(){
            $('.delete').click(function(){
                event.preventDefault();
                var answer = confirm('Are you sure? Click Ok to delete.');
                if(answer){
                    window.location = $(this).attr('href');
                }
            });
        })
    </script>
@endpush
