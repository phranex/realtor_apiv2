@extends('layouts.admin')


@section('content')
    <!-- begin row -->
    <div class="row">
        <div class="col-md-12 m-b-30">
            <!-- begin page title -->
            <div class="d-block d-sm-flex flex-nowrap align-items-center">
                <div class="page-title mb-2 mb-sm-0">
                    <h1>Dashboard</h1>
                </div>
                {{-- <div class="ml-auto d-flex align-items-center">
                    <nav>
                        <ol class="breadcrumb p-0 m-b-0">
                            <li class="breadcrumb-item">
                                <a href="index.html"><i class="ti ti-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                Dashboard
                            </li>
                            <li class="breadcrumb-item active text-primary" aria-current="page">Real Estate</li>
                        </ol>
                    </nav>
                </div> --}}
            </div>
            <!-- end page title -->
        </div>
    </div>
    <!-- end row -->
    <!-- start real estate contant -->
    <div class="row">
        <div class="col-xs-6 col-xxl-3 m-b-30">
            <div class="card card-statistics h-100 m-b-0 bg-pink">
                <div class="card-body">
                    <h2 class="text-white mb-0">{{getNumberFormat(@$total_users)}}</h2>
                    <p class="text-white">Users</p>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-xxl-3 m-b-30">
            <div class="card card-statistics h-100 m-b-0 bg-primary">
                <div class="card-body">
                    <h2 class="text-white mb-0">{{getNumberFormat(@$total_reviews)}}</h2>
                    <p class="text-white">Reviews </p>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-xxl-3 m-b-30">
            <div class="card card-statistics h-100 m-b-0 bg-orange">
                <div class="card-body">
                    <h2 class="text-white mb-0">{{currencyConverter(@$total_revenue)}}</h2>
                    <p class="text-white">Total Revenue </p>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-xxl-3 m-b-30">
            <div class="card card-statistics h-100 m-b-0 bg-info">
                <div class="card-body">
                    <h2 class="text-white mb-0">{{getNumberFormat(@$total_subscriptions)}}</h2>
                    <p class="text-white">Total Subscriptions</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-12 ">
                <div class="card card-statistics">
                    <div class="card-header">
                        <div class="card-heading">
                            <h4 class="card-title">10 most Recent Registrations
                            <Small class="text-bold">Total Users: {{count($users)}}</Small>
                            </h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">FullName</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Date Registered</th>
                                        {{-- <th scope="col">Phone Number</th> --}}
                                        <th scope="col">Verified</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @isset($users)
                                        @if(count($users))
                                            @foreach($users as $user)
                                                <tr>
                                                    <td>{{$loop->iteration}}</td>
                                                    <td>
                                                        <div>


                                                        <img src="@if(isset($user->avatar['full_path'])) {{$user->avatar['full_path']}} @else {{asset('img/avatar.jpg')}}  @endif" class="img-fluid " style="width:30px; height:auto;border-radius:50%" />

                                                        {{$user->first_name}} {{$user->last_name}}
                                                        </div>
                                                    </td>
                                                    <td>{{$user->email}}</td>
                                                    <td>{{getDateFormat($user->created_at, 'd-M-Y')}}</td>
                                                    <td>
                                                        @if($user->verified)
                                                            <small style="border:1px solid red;border-radius:12px;padding:2px"><i class="fa fa-check text-success"></i> Not Verified</small>
                                                        @else
                                                        <small style="border:1px solid red;border-radius:12px;padding:2px 5px"><i  class="fa fa-times text-danger"></i> Not Verified</small>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="{{route('admin.user.show',$user->id)}}" class="btn btn-xs btn-outline-primary"> <i class="fa fa-eye"></i> View</a>
                                                        <a class="btn btn-xs btn-outline-danger"> <i class="fa fa-lock"></i> Restrict</a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6">
                                                    <div class="alert text-center ">No Realtor found</div>
                                                </td>
                                            </tr>
                                        @endif

                                    @endisset
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xxl-6 m-b-30">
            <div class="card card-statistics h-100 m-b-0">
                <div class="card-header d-flex justify-content-between">
                    <div class="card-heading">
                        <h4 class="card-title">Income analysis</h4>
                    </div>
                    {{-- <div class="dropdown">
                        <a class="p-2" href="#!" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fe fe-more-horizontal"></i>
                        </a>
                        <div class="dropdown-menu custom-dropdown dropdown-menu-right p-4">
                            <h6 class="mb-1">Action</h6>
                            <a class="dropdown-item" href="#!"><i class="fa-fw fa fa-file-o pr-2"></i>View reports</a>
                            <a class="dropdown-item" href="#!"><i class="fa-fw fa fa-edit pr-2"></i>Edit reports</a>
                            <a class="dropdown-item" href="#!"><i class="fa-fw fa fa-bar-chart-o pr-2"></i>Statistics</a>
                            <h6 class="mb-1 mt-3">Export</h6>
                            <a class="dropdown-item" href="#!"><i class="fa-fw fa fa-file-pdf-o pr-2"></i>Export to PDF</a>
                            <a class="dropdown-item" href="#!"><i class="fa-fw fa fa-file-excel-o pr-2"></i>Export to CSV</a>
                        </div>
                    </div> --}}
                </div>
                <div class="card-body">
                        <div class="text-right">
                            <span class="mr-3"><i class="fa fa-square pr-1 text-primary"></i> Revenue overview for the last 6 months</span>
                            {{-- <span><i class="fa fa-square pr-1 text-info"></i> Page view</span> --}}
                        </div>
                        <div class="apexchart-wrapper">
                            <div id="subscription_chart"></div>
                        </div>
                    </div>
            </div>
        </div>
        <div class="col-xxl-6 m-b-30">
            <div class="card card-statistics h-100 mb-0">
                <div class="card-header">
                    <h4 class="card-title">Revenue overview</h4>
                </div>
                <div class="card-body">


                    <h6 class="card-title">Income by Subscription plans</h6>
                    <div class="row">
                        @if(isset($paymentStat))
                            @if(count($paymentStat))
                                @foreach ($paymentStat as $key => $value)
                                <div class="col-12 col-xs-3">
                                        <span>{{$key}}: <b>{{currencyConverter($value)}}</b></span>
                                        <div class="progress my-3" style="height: 4px;">
                                            <div class="progress-bar {{progressBarDecorator($value,$total_revenue)}} " role="progressbar" style="width: {{percentAdder($value,$total_revenue)}};" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                @endforeach

                            @else
                                <div class="col-12 alert text-center">
                                    Please create a pricing plan to see statistics. Click <a href="{{route('admin.packages')}}" class="btn btn-link btn-sm">here</a> to create a package.
                                </div>
                            @endif
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


@push('scripts')

    <script>
        var subscription_chart = jQuery('#subscription_chart')
                if (subscription_chart.length > 0) {
                    var options = {
                            chart: {
                                height: 350,
                                type: 'line',
                                shadow: {
                                    enabled: true,
                                    color: '#000',
                                    top: 18,
                                    left: 7,
                                    blur: 10,
                                    opacity: 1
                                },
                                toolbar: {
                                    show: false
                                }
                            },
                            colors: ['#8E54E9', '#4776E6'],
                            dataLabels: {
                                enabled: true,
                            },
                            stroke: {
                                curve: 'smooth'
                            },
                            series: [{
                                    name: "Revenue ($)",
                                    data: [
                                        @if(isset($income_stat))
                                            @foreach($income_stat as $key => $value)
                                                '{{$value}}',
                                            @endforeach
                                        @endif
                                    ]
                                },

                            ],
                            grid: {
                                borderColor: '#dee0ea',
                                row: {
                                    colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                                    opacity: 0.5
                                },
                            },
                            markers: {
                                size: 6
                            },
                            xaxis: {
                                categories: [
                                    @if(isset($income_stat))
                                        @foreach($income_stat as $key => $value)
                                            '{{$key}}',
                                        @endforeach
                                    @endif
                                ]
                            },
                            // yaxis: {
                            //     min: 1,
                            //     max:
                            // },
                            legend: {
                                show: false,
                                position: 'top',
                                horizontalAlign: 'right',
                                floating: true,
                                offsetY: -25,
                                offsetX: -5
                            }
                        }

                    var chart = new ApexCharts(
                        document.querySelector("#subscription_chart"),
                        options
                    );

                    chart.render();
                }
    </script>
@endpush
