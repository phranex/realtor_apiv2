@extends('layouts.admin')


@section('content')
<div class="col-12 ">
    <div class="card card-statistics">
        <div class="card-header">
            <div class="card-heading">
                <h4 class="card-title">Realtors on the Platform
                <Small class="text-bold">Total: {{count($users)}}</Small>
                </h4>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table mb-0">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">FullName</th>
                            <th scope="col">Email</th>
                            <th scope="col">Date Registered</th>
                            {{-- <th scope="col">Phone Number</th> --}}
                            <th scope="col">Verified</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @isset($users)
                            @if(count($users))
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$user->first_name}} {{$user->last_name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{getDateFormat($user->created_at, 'd-M-Y')}}</td>
                                        <td>
                                            @if($user->verified)
                                                <small style="border:1px solid red;border-radius:12px;padding:2px"><i class="fa fa-check text-success"></i> Not Verified</small>
                                            @else
                                            <small style="border:1px solid red;border-radius:12px;padding:2px 5px"><i  class="fa fa-times text-danger"></i> Not Verified</small>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{route('admin.user.show',$user->id)}}" class="btn btn-xs btn-outline-primary"> <i class="fa fa-eye"></i> View</a>
                                            <a class="btn btn-xs btn-outline-danger"> <i class="fa fa-lock"></i> Restrict</a>
                                        </td>

                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6">
                                        <div class="alert text-center ">No Realtor found</div>
                                    </td>
                                </tr>
                            @endif

                        @endisset
                    </tbody>
                </table>
                {{$users->links()}}
            </div>
        </div>
    </div>
</div>

@endsection
