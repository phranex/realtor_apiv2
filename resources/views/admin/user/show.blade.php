@extends('layouts.admin')
@push('styles')
    <style>
    .heading {
  font-size: 25px;
  margin-right: 25px;
}

.fa {
  font-size: 25px;
}

.checked {
  color: orange;
}

/* Three column layout */
.side {
  float: left;
  width: 15%;
  margin-top: 10px;
}

.middle {
  float: left;
  width: 70%;
  margin-top: 10px;
}

/* Place text to the right */
.right {
  text-align: right;
}

/* Clear floats after the columns */
.r-row:after {
  content: "";
  display: table;
  clear: both;
}

/* The bar container */
.bar-container {
  width: 100%;
  background-color: #f1f1f1;
  text-align: center;
  color: white;
}

/* Individual bars */
.bar-5 {width: 60%; height: 18px; background-color: #4CAF50;}
.bar-4 {width: 30%; height: 18px; background-color: #2196F3;}
.bar-3 {width: 10%; height: 18px; background-color: #00bcd4;}
.bar-2 {width: 4%; height: 18px; background-color: #ff9800;}
.bar-1 {width: 15%; height: 18px; background-color: #f44336;}

/* Responsive layout - make the columns stack on top of each other instead of next to each other */
@media (max-width: 400px) {
  .side, .middle {
    width: 100%;
  }
  /* Hide the right column on small screens */
  .right {
    display: none;
  }
}

    </style>
@endpush


@section('content')
<div class="row account-contant">
    <div class="col-12">
        <div class="card card-statistics">
            <div class="card-body p-0">
                <div class="row no-gutters">
                    <div class="col-xl-3 pb-xl-0 pb-5 border-right">
                        <div class="page-account-profil pt-5">
                            <div class="profile-img text-center rounded-circle">
                                <div class="pt-5">
                                        <div class="bg-img m-auto">
                                                <img src="{{$user->avatar['full_path']}}" class="img-fluid" alt="users-avatar">
                                                </div>

                                    <div class="profile pt-4">
                                    <h4 class="mb-1">{{$user->first_name}} {{$user->last_name}}</h4>
                                    <p><i class="fa fa-envelope"></i> <a href="mailto:{{$user->email}}">{{$user->email}}</a></p>
                                    <small>{{trans('Registered')}} {{when($user->created_at)}}</small>
                                    </div>
                                </div>
                            </div>

                            <div class="py-5 profile-counter">
                                    <ul class="nav justify-content-center text-center">
                                        <li class="nav-item border-right px-3">
                                            <div>
                                                <h4 class="mb-0">{{count($user->invitations)}}</h4>
                                                <p>Requests</p>
                                            </div>
                                        </li>

                                        <li class="nav-item border-right px-3">
                                            <div>
                                                <h4 class="mb-0">{{count($user->contacts)}}</h4>
                                                <p>Contacts</p>
                                            </div>
                                        </li>

                                        <li class="nav-item px-3">
                                            <div>
                                                <h4 class="mb-0">{{count($user->reviews)}}</h4>
                                                <p>Reviews</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                            <div class="profile-btn text-center">
                            <div><a href="{{env('FRONT_END_BASE_URL')}}u/{{$user->setting->business_name}}" target="_blank" class="btn btn-light text-primary mb-2">View Reviews</a></div>
                                <div>
                                        {{-- @if($user->blocked)
                                        <a href='{{route('admin.users.unblock',$user->id)}}' class='btn btn-xs btn-outline-warning'>{{trans('Unblock Access')}}</a>
                                        @else
                                            <a href='{{route('admin.users.block',$user->id)}}' class='btn btn-xs btn-outline-danger'>{{trans('Block Access')}}</a>
                                        @endif --}}

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-9 col-md-9 col-12 ">
                        <div class=" tabs-contant">
                                <div class="col-12">
                                    <div class=" card-statistics">

                                        <div class="card-body">
                                            <div class="tab round">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                    <a class="nav-link show active" id="home-07-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="home-07" aria-selected="true">  <span class="d-none d-md-block">Profile</span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        {{-- <a class="nav-link show" id="profile-07-tab" data-toggle="tab" href="#password" role="tab" aria-controls="profile-07" aria-selected="false"><span class="d-none d-md-block">Change Password</span> </a> --}}
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link show" id="portfolio-07-tab" data-toggle="tab" href="#preferences" role="tab" aria-controls="portfolio-07" aria-selected="false"> <span class="d-none d-md-block">Statistics</span> </a>
                                                    </li>

                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane fade py-3 active show" id="profile" role="tabpanel" aria-labelledby="home-07-tab">
                                                            {{-- <div class="card-body">
                                                                    <div class="media">
                                                                        <img class="mr-3 mb-3 mb-xxs-0 img-fluid" src="http://localhost:1000/dashboard/assets/img/avtar/01.jpg" alt="image">
                                                                        <div class="media-body">
                                                                            <h5 class="mt-0">Savvyone</h5>
                                                                            Since there is not an "all the above" category, I'll take the opportunity to enthusiastically congratulate you on the very high quality, "user-friendly" documentation.
                                                                        </div>
                                                                    </div>
                                                                </div> --}}

                                                                <div class="p-4">
                                                                        <form method="post" action="#" enctype="multipart/form-data">
                                                                            @csrf

                                                                            <div class="form-row">
                                                                                <div class="form-group col-md-12">
                                                                                    <label for="name1">Full Name</label>
                                                                                    <input type="text" class="form-control"  id="name" readonly required name="name" value="{{getFullname($user)}}">
                                                                                </div>
                                                                                <div class="form-group col-md-12">
                                                                                        <label for="title1">Business Name</label>
                                                                                    <input type="text" class="form-control" readonly required name="business_name" id="business_name" value="{{str_replace('_', ' ',@$user->setting->business_name)}}">
                                                                                    </div>
                                                                                <div class="form-group col-md-12">
                                                                                    <label for="title1">Title</label>
                                                                                    <input type="text" class="form-control" readonly required name="business_name" id="business_name" value="{{@$user->setting->title}}">                                                                                </div>
                                                                                <div class="form-group col-md-12">
                                                                                    <label for="phone1">Phone Number</label>
                                                                                    <input type="text" class="form-control" readonly required name="business_name" id="business_name" value="{{@$user->setting->phone_number}}">
                                                                                </div>
                                                                                <div class="form-group col-md-12">
                                                                                    <label for="email1">Email</label>
                                                                                    <input type="text" class="form-control" readonly required name="business_name" id="business_name" value="{{@$user->setting->email}}">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="add1">Address</label>
                                                                                <input type="text" class="form-control" readonly required name="business_name" id="business_name" value="{{@$user->setting->address}}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="add2">Address 2</label>
                                                                                <input type="text" class="form-control" readonly required name="business_name" id="business_name" value="{{@$user->setting->address2}}">
                                                                            </div>

                                                                            <div class="form-group">

                                                                                    <label class="mb-1">Birthday</label>

                                                                                    <input type="text" class="form-control" readonly required name="business_name" id="business_name" value="{{getDateFormat(@$user->setting->date_of_birth,'d-M-Y')}}">

                                                                            </div>
                                                                            <div class="form-row">
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="inputState">City</label>
                                                                                    <input type="text" class="form-control" readonly required name="business_name" id="business_name" value="{{@$user->setting->city}}">
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="inputState">State</label>
                                                                                    <input type="text" class="form-control" readonly required name="business_name" id="business_name" value="{{@$user->setting->state}}">
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                        <label for="inputState">Country</label>
                                                                                        <input type="text" class="form-control" readonly required name="business_name" id="business_name" value="{{@$user->setting->country}}">
                                                                                    </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="inputZip">Zip</label>
                                                                                    <input type="text" class="form-control" readonly required name="business_name" id="business_name" value="{{@$user->setting->zip}}">
                                                                                </div>
                                                                            </div>

                                                                        </form>
                                                                    </div>



                                                    </div>

                                                    <div class="tab-pane fade py-3" id="preferences" role="tabpanel" aria-labelledby="portfolio-07-tab">
                                                        <div class="ro" >

                                                                <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <div class="car card-statistics">
                                                                                <div class="row">
                                                                                    <div class="col-xl-6  m-b-30">
                                                                                        <div class="card card-statistics h-100 mb-0">
                                                                                            <div class="card-header">
                                                                                                <h4 class="card-title">User Rating</h4>
                                                                                            </div>
                                                                                            <div class="card-body">
                                                                                                <div class="row">
                                                                                                        <div class="col-12">
                                                                                                        @for($i = 0; $i < $ratings['avg_rating']; $i++)

                                                                                                            <span class="fa fa-star checked"></span>
                                                                                                        @endfor
                                                                                                        @for($i = 0; $i < 5 - $ratings['avg_rating']; $i++)

                                                                                                        <span class="fa fa-star"></span>
                                                                                                        @endfor


                                                                                                        <p>{{number_format(@$ratings['avg_rating'],1)}} average based on {{@$ratings['total']}} reviews.</p>
                                                                                                        <hr style="border:3px solid #f1f1f1">

                                                                                                        <div class="r-row">
                                                                                                            @php
                                                                                                            $arr = array_column(json_decode($ratings['rating_scores_count'],true), 'rating_score');
                                                                                                            $width = 0;

                                                                                                            @endphp
                                                                                                            @for($i = 5; $i > 0; $i--)
                                                                                                                @php $index = array_search($i, $arr); @endphp
                                                                                                            @if($index !== false)
                                                                                                                @php
                                                                                                                    $rate = $ratings['rating_scores_count'][$index]['count'];
                                                                                                                    $tot = $ratings['total'];
                                                                                                                    $width = $rate / $tot * 100;
                                                                                                                    $width = $width.'%';
                                                                                                                @endphp
                                                                                                            <div class="side">
                                                                                                                <div>{{$i}} star</div>
                                                                                                            </div>
                                                                                                            <div class="middle">
                                                                                                                <div class="bar-container">
                                                                                                                <div style='width:{{$width}}' class="bar-{{$i}}"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="side right">
                                                                                                            <div>{{$ratings['rating_scores_count'][$index]['count']}}</div>
                                                                                                            </div>
                                                                                                            @else
                                                                                                            <div class="side">
                                                                                                                    <div>{{$i}} star</div>
                                                                                                                </div>
                                                                                                                <div class="middle">
                                                                                                                    <div class="bar-container">
                                                                                                                    <div style='width:0px' class="bar-{{$i}} "></div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="side right">
                                                                                                                <div>0</div>
                                                                                                            </div>

                                                                                                            @endif

                                                                                                            @endfor

                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-6  m-b-30">
                                                                                        <div class="card card-statistics h-100 mb-0 widget-income-list">
                                                                                            <div class="card-body d-flex align-itemes-center">
                                                                                                    <div class="media align-items-center w-100">
                                                                                                        <div class="text-left">
                                                                                                            <h3 class="mb-0"> {{number_format(@$analytics['contacts']['total'])}}  </h3>
                                                                                                            <span>Total Contacts


                                                                                                            </span>

                                                                                                        </div>
                                                                                                        <div class="img-icon bg-primary ml-auto">
                                                                                                            <i class="ti ti-tag text-white"></i>
                                                                                                        </div>
                                                                                                    </div>
                                                                                            </div>
                                                                                            <div class="card-body d-flex align-itemes-center">
                                                                                                <div class="media align-items-center w-100">
                                                                                                    <div class="text-left">
                                                                                                        <h3 class="mb-0">{{number_format(@$analytics['reviews']['total'])}} </h3>
                                                                                                        <span>Total Reviews</span>
                                                                                                    </div>
                                                                                                    <div class="img-icon bg-pink ml-auto">
                                                                                                        <i class="ti ti-user text-white"></i>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>


                                                                                            <div class="card-body d-flex align-itemes-center">
                                                                                                <div class="media align-items-center w-100">
                                                                                                    <div class="text-left">
                                                                                                        <h3 class="mb-0">{{number_format(@$analytics['avg_rating'],1)}}  </h3>
                                                                                                        <span>Average rating</span>
                                                                                                    </div>
                                                                                                    <div class="img-icon bg-info ml-auto">
                                                                                                        <i class="ti ti-slice text-white"></i>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                </div>

                                                                <div class="row">
                                                                        <div class="col-xxl-12 m-b-30">
                                                                                <div class="card card-statistics h-100 mb-0">
                                                                                    <div class="card-header">
                                                                                        <h4 class="card-title">Reviews Report (By Month)</h4>
                                                                                    </div>

                                                                                    <div class="chart-container">
                                                                                            <canvas id="chart-legend-top"></canvas>
                                                                                    </div>
                                                                                </div>


                                                                        </div>
                                                                </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
<script src="https://www.chartjs.org/samples/latest/utils.js"></script>
<script>

		var color = Chart.helpers.color;
		function createConfig(legendPosition, colorName) {
			return {
				type: 'line',
				data: {
					labels:[
                    @if(isset($analytics['reviews_count']))
                        @foreach($analytics['reviews_count'] as $key => $value)
                            '{{$key}}',

                        @endforeach
                    @endif
                    ],
					datasets: [{
						label: 'Reviews',
						data: [
                            @if(isset($analytics['reviews_count']))
                                @foreach($analytics['reviews_count'] as $key => $value)
                                    '{{$value}}',
                                @endforeach
                            @endif
						],
						backgroundColor: color(window.chartColors[colorName]).alpha(0.5).rgbString(),
						borderColor: window.chartColors[colorName],
						borderWidth: 1
					}]
				},
				options: {
					responsive: true,
					legend: {
						position: legendPosition,
					},
					scales: {
						xAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Month'
							}
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Number of Reviews'
							}
						}]
					},
					title: {
						display: true,
						text: 'Review/Month Analysis: ' + legendPosition
					}
				}
			};
		}

		window.onload = function() {
			[{
				id: 'chart-legend-top',
				legendPosition: 'top',
				color: 'red'
			}].forEach(function(details) {
				var ctx = document.getElementById(details.id).getContext('2d');
				var config = createConfig(details.legendPosition, details.color);
				new Chart(ctx, config);
			});
		};
	</script>
@endpush

