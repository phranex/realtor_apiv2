@extends('layouts.admin')

@push('styles')
    <Style>
    .select2-container--default .select2-selection--multiple .select2-selection__rendered li{
        margin-bottom: 2px;
    display: inline-block;
    padding: .6em .7em;
    color:White;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice{
        background-color: #8e54e9;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove{
        color:White;
    }
    </Style>

@endpush


@section('content')
<div class="col-12 ">
    <div class="card card-statistics">
        <div class="card-header">
            <div class="card-heading">
                <h4 class="card-title">{{config('app.name')}} Packages

                </h4>
            </div>
        </div>
        <div class="card-body">
            <form method='post' action="{{route('admin.package.update',$package->id)}}" class="form-horizontal  mt-4">
                @csrf
                <div class="form-group">
                    <label>Name <span class="help"></span></label>
                <input required name='name' required type="text" class="form-control" value="@if(old('name')){{old('name')}}@else {{$package->name}}  @endif">
                </div>
                <div class="form-group">
                        <label>Monthly Cost <span class="help"></span></label>
                    <input required name='price' step="0.01" required type="number" class="form-control" value="@if(old('price'))  {{old('price')}}@else{{$package->price}}@endif">
                </div>
                    {{-- <div class="form-group  select-wrapper">
                            <label>Select Features <span class="help"></span></label>
                            <Select multiple="multiple" required name="features[]" class="js-basic-multiple form-control col-md-6">
                                    <optgroup label="Select Features">
                                @if(isset($features))
                                    @if(count($features))
                                        @foreach($features as $feature)
                                            <option @if(in_array($feature->id, $pf)) selected @endif value="{{$feature->id}}">{{$feature->name}}</option>
                                        @endforeach
                                    @endif
                                    </optgroup>
                                @endif
                            </Select>
                        </div> --}}
                        <div class="">
                                <p class="text-center">Features <br/>
                                        <small class="text-center">Add features to this package</small>
                                    </p><hr/>

                                @if(isset($features))
                                    @if(count($features))
                                        <div class="row">
                                        @foreach($features as $feature)
                                            <div class="form-group col-md-3">
                                                    <input hidden class="form-control" readonly value="{{$feature->id}}" name="features[]"  />
                                                <input class="form-control" readonly value="{{$feature->name}}"  />

                                                @if($feature->type == 1)
                                                <div style='margin-top:15px'>
                                                <small>Enter the maximum value for the package. To signify unlimited, please enter a number greater than 100000</small>
                                                <input required type="number" value="{{getPackageValue($feature->id,$package->id)}}" name="feature_value[]" class="form-control" />
                                                </div>
                                                @else
                                                <div style='margin-top:15px'>
                                                        <Small><span class="help">Do you wish to apply this feature to this package?</span></small>
                                                        <div  class="custom-control custom-radio custom-control-inline">

                                                                                        <input required   @if(getPackageValue($feature->id,$package->id)) checked  @endif type="radio" id="yes" value="true" name="feature_value[]" value='true' class="custom-control-input">

                                                                                        <label class="custom-control-label" for="yes">Yes</label>

                                                                                    </div>
                                                                                    <div class="custom-control custom-radio custom-control-inline">
                                                                                        <input required @if(!getPackageValue($feature->id,$package->id)) checked  @endif type="radio" id="no" name="feature_value[]" value="false" class="custom-control-input">

                                                                                        <label class="custom-control-label" for="no">No</label>

                                                                                    </div></div>
                                                @endif
                                            </div>
                                        @endforeach
                                        </div>
                                    @endif

                                @endif

                            </div>



                <div class="form-group m-t-15">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
