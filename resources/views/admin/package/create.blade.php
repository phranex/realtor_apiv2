@extends('layouts.admin')

@push('styles')
    <Style>
    .select2-container--default .select2-selection--multiple .select2-selection__rendered li{
        margin-bottom: 2px;
    display: inline-block;
    padding: .6em .7em;
    color:White;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice{
        background-color: #8e54e9;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove{
        color:White;
    }
    </Style>

@endpush


@section('content')
<div class="col-12 ">
    <div class="card card-statistics">
        <div class="card-header">
            <div class="card-heading">
                <h4 class="card-title">Add Package

                </h4>
            </div>
        </div>
        <div class="card-body">
            <form method='post' action="{{route('admin.package.store')}}" class="form-horizontal  mt-4">
                @csrf
                <div class="row">
                    <div class="form-group col-md-6">
                        <label>Name <span class="help"></span></label>
                    <input required name='name' required type="text" class="form-control" value="@if(old('name'))  {{old('name')}}  @endif">
                    </div>
                    <div class="form-group col-md-6">
                            <label>Monthly Cost <span class="help"></span></label>
                        <input required name='price' step="0.01" required type="number" class="form-control" value="@if(old('price'))  {{old('price')}}  @endif">
                    </div>
                </div>

                    {{-- <div class="form-group  select-wrappe">
                            <label>Select Features <span class="help"></span></label>
                            <div id='select-box'>
                                    <Select    required name="features[]" class="js-basic-multiple select-feature form-control col-md-4">
                                            <optgroup label="Select Features">
                                                <option value=''></option>
                                        @if(isset($features))
                                            @if(count($features))
                                                @foreach($features as $feature)
                                                    <option data-type={{$feature->type}} value="{{$feature->id}}">{{$feature->name}}</option>
                                                @endforeach
                                            @endif
                                            </optgroup>
                                        @endif
                                    </Select>
                            </div>

                    </div> --}}
                <div class="">
                    <p class="text-center">Features <br/>
                            <small class="text-center">Add features to this package</small>
                        </p><hr/>

                    @if(isset($features))
                    @if(count($features))
                        <div class="row">
                        @foreach($features as $feature)
                            <div class="form-group col-md-3">
                                    <input hidden class="form-control" readonly value="{{$feature->id}}" name="features[]"  />
                                <input class="form-control" readonly value="{{$feature->name}}"  />

                                @if($feature->type == 1)
                                <div style='margin-top:15px'>
                                <small>Enter the maximum value for the package. To signify unlimited, please enter a number greater than 100000</small>
                                <input required type="number" name="feature_value[]" class="form-control" />
                                </div>
                                @else
                                <div style='margin-top:15px'>
                                        <Small><span class="help">Do you wish to apply this feature to this package?</span></small>
                                         <div  class="custom-control custom-radio custom-control-inline">
                                                                        <input required type="radio" id="yes" name="feature_value[]" value='true' class="custom-control-input">

                                                                        <label class="custom-control-label" for="yes">Yes</label>

                                                                    </div>
                                                                    <div class="custom-control custom-radio custom-control-inline">
                                                                        <input required type="radio" id="no" name="feature_value[]" value='false' class="custom-control-input">

                                                                        <label class="custom-control-label" for="no">No</label>

                                                                    </div></div>
                                @endif
                            </div>
                        @endforeach
                        </div>
                    @endif

                @endif

                </div>



                <div class="form-group m-t-15">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection


@push('scripts')

    <script>
        $(document).ready(function(){
            var num_html = ` <div class="form-group">

                    <Small><span class="help">You signify unlimited, enter a value greater than 100000</span></small>
                <input  name='feature_value' required type="number" class="form-control" value="">
                </div>`;
            var conditional_html = `<div style='margin-top:15px'>
            <Small><span class="help">Do you wish to apply this feature to this package?</span></small>
             <div  class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="yes" name="feature_value[]" value='1' class="custom-control-input">

                                            <label class="custom-control-label" for="yes">Yes</label>

                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="no" name="feature_value[]" value='0' class="custom-control-input">

                                            <label class="custom-control-label" for="no">No</label>

                                        </div></div>`;
            $('.select-feature').change(function(){
                var value = $(this).val();
                if(value.length){
                    var type = $(this).children(':selected').attr('data-type');
                    if(type == 1){
                        $(this).after(num_html);
                    }else{
                        $(this).after(conditional_html);
                    }
                }
            });
        })
    </script>
@endpush
