@extends('layouts.admin')


@section('content')
<div class="row">
        <div class="col-12 ">
                <div class="card card-statistics">
                    <div class="card-header">
                        <div class="card-heading">
                            <h4 class="card-title">{{config('app.name')}} Packages
                            {{-- <Small class="text-bold">Total: {{count($users)}}</Small> --}}
                            </h4>
                            <div class="text-right">
                                <a class="btn btn-sm btn-primary" href="{{route('admin.package.create')}}">Add Package</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
</div>


<div class="row">
    @isset($packages)
        @if(count($packages))
            @foreach($packages as $package)
            <div  class="col-xl-3 col-md-6">
                <div  class="card card-statistics text-center py-3">
                    <div class="card-body pricing-content">
                        <div class="pricing-content-card">
                        <h5>{{$package->name}}</h5>
                            <h2 class="text-primary pt-3">{{currencyConverter($package->price)}}</h2>
                            <p class="text-primary pb-3">/ Monthly</p>
                            <div class="text-center">Features</div>
                            <hr/>
                            <ul class="py-2">
                                @if(count($package->qualities))
                                    @foreach($package->qualities as $quality)
                                        <li>
                                            @if($quality->feature->type == 1)
                                                <strong>
                                                    @if($quality->value >= 100000)
                                                        Unlimited
                                                    @else
                                                    {{$quality->value}}
                                                    @endif
                                                </strong> {{$quality->feature->name}}
                                            @else
                                                @if($quality->value)
                                                    <i class="fa fa-check text-success"></i>  {{$quality->feature->name}}
                                                @else
                                                    <i class="fa fa-times text-danger"></i>  {{$quality->feature->name}}
                                                @endif
                                            @endif
                                        </li>
                                    @endforeach
                                @endif

                            </ul>
                            <div class="pt-2">
                                <a href="{{route('admin.package.edit', $package->id)}}" class="btn btn-primary btn-round btn-sm"> <i class="fa fa-edit"></i> Edit</a>
                                <a href="{{route('admin.package.delete',$package->id)}}" class="btn btn-danger delete btn-round btn-sm"> <i class="fa fa-trash"></i> Remove</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        @endif


    @endisset
</div>

@endsection

@push('scripts')

    <script>
        $(document).ready(function(){
            $('.delete').click(function(){
                event.preventDefault();
                var answer = confirm('Are you sure? Click Ok to delete.');
                if(answer){
                    window.location = $(this).attr('href');
                }
            });
        })
    </script>
@endpush
