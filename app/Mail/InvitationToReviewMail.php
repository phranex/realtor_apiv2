<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvitationToReviewMail extends Mailable
{
    use Queueable, SerializesModels;
    public $contact;
    public $mess;
    public $url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contact,$message,$url)
    {
        //
        $this->contact = $contact;
        $this->mess = $message;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.api.invitation-to-review')->subject('You have been invited!!');
    }
}
