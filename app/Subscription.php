<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Package;

class Subscription extends Model
{
    //
    // public function create($email)
    // {
    //     # code...
    //     $this->email = $email;
    //     return $this->save();
    // }


    public static function getTotalRevenue()
    {
        # code...
        $revenue = 0;
        $subscriptions = self::all();
        if(count($subscriptions)){
            foreach ($subscriptions as $subscription) {
                # code...
                $revenue += Package::where('name', $subscription->name)->pluck('price')->first();
            }
        }

        return $revenue;


    }
}
