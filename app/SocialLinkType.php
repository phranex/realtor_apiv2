<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialLinkType extends Model
{
    //
    public function create($request)
    {
        # code...
        $this->name = $request->name;
        $this->isRequired = ($request->isRequired)? 1: 0;
        $this->description = $request->description;
        return $this->save();
    }
}
