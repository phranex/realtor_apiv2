<?php

use App\Quality;

function apiResponse($status,$message,$data){
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }


    function getImageLink($link){
        return env('IMAGE_URL').'/storage/'.$link;
    }

    function sendInvitation($contact,$message,$url){
        //send invitation to email
        logger($message);
        if(isset($contact->fullname)){
            //
            Mail::to($contact->email)->send(new \App\Mail\InvitationToReviewMail($contact,$message,$url));
        }else{
            Mail::to($contact)->send(new \App\Mail\InvitationToReviewMail($contact,$message,$url));
        }

    }

    function getFullname($user){
        echo ucwords($user->first_name. ' '. $user->last_name);
    }

    function when($time){
        echo \Carbon\Carbon::parse($time)->diffForHumans();
    }


    function getNumberFormat($num){
        return number_format($num);
    }

    function getDateFormat($time,$format){
        echo \Carbon\Carbon::parse($time)->format($format);
    }

    function sendReplyToReviewer($email,$review, $reply){

    }

    function createIdentifier(){
       $x  = auth()->id().'-'.str_replace(' ', '|',auth()->user()->first_name).'_'.str_replace(' ', '|',auth()->user()->last_name);
        return base64_encode($x);
    }

    function currencyConverter($amt){
        echo '$'. number_format($amt, 2);
    }

    function getPackageValue($feature_id, $package_id){
       return $quality = Quality::where('feature_id',$feature_id)
                                    ->where('package_id', $package_id)
                                    ->pluck('value')
                                    ->first();

    }

    function progressBarDecorator($value, $total){
        $x = percentage($value, $total);
        switch ($x) {
            case $x >= 80:
                # code...
                echo 'bg-success';
                break;
            case $x >= 70 && $x < 70:
                # code...
                echo '';
                break;
            case $x >= 40 && $x < 70:
                # code...
                echo 'bg-info';
                break;
            case $x < 40:
                # code...
                echo 'bg-danger';
                break;

            default:
                # code...
                break;
        }
    }

    function percentAdder($value, $total){
            return percentage($value, $total).'%';
    }

    function percentage($value, $total){
        if(is_numeric($value) && is_numeric($total) && $value > 0 && $total > 0){
            return $value / $total * 100;
        }
        return 0;
    }

    function  getFeatureType(){

    }

    function  setFeatureType(){

    }







?>
