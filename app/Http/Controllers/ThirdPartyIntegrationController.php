<?php

namespace App\Http\Controllers;

use App\ThirdPartyIntegration;
use Illuminate\Http\Request;

class ThirdPartyIntegrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('integration.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required',
            'title' => 'required'
        ]);
        $tpi = new \App\ThirdPartyIntegration;
        $tpi->create($request);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ThirdPartyIntegration  $thirdPartyIntegration
     * @return \Illuminate\Http\Response
     */
    public function show(ThirdPartyIntegration $thirdPartyIntegration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ThirdPartyIntegration  $thirdPartyIntegration
     * @return \Illuminate\Http\Response
     */
    public function edit(ThirdPartyIntegration $thirdPartyIntegration)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ThirdPartyIntegration  $thirdPartyIntegration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ThirdPartyIntegration $thirdPartyIntegration)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ThirdPartyIntegration  $thirdPartyIntegration
     * @return \Illuminate\Http\Response
     */
    public function destroy(ThirdPartyIntegration $thirdPartyIntegration)
    {
        //
    }
}
