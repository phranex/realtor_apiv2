<?php

namespace App\Http\Controllers;

use App\PersonalSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class PersonalSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $required = [
            'title','email','business_name','address','phone_number','date_of_birth','city','state','country'
        ];
        $form =request()->all();

        if(validate_request($form,$required)){
          //check if user has updated hsi profile before
            $personal_setting = PersonalSetting::where('user_id', auth()->id())->first();
            if(empty($personal_setting)){
                $personal_setting = new PersonalSetting;
            }
            if($personal_setting->createOrEdit($form)){
                return response()->json([
                    'status' => 1,
                    'message' => __('Profile Updated'),
                    'data' => []
                ]);
            }else{
                return response()->json([
                    'status' => 0,
                    'message' => __('yes'),
                    'data' => []
                ]);
            }

        }


        return response()->json([
            'status' => 0,
            'message' => __('Please fill fields correctly'),
            'data' => []
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PersonalSetting  $personalSetting
     * @return \Illuminate\Http\Response
     */
    public function show(PersonalSetting $personalSetting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PersonalSetting  $personalSetting
     * @return \Illuminate\Http\Response
     */
    public function edit(PersonalSetting $personalSetting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PersonalSetting  $personalSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PersonalSetting $personalSetting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PersonalSetting  $personalSetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(PersonalSetting $personalSetting)
    {
        //
    }
    public function uploadAvatar()
    {
        # code...

        if(request()->hasFile('avatar')){

            //check if user already has  an avatar
            $avatar = \App\Avatar::where('user_id', auth()->id())->first();
            if(!isset($avatar)){
                // return "true";
                $avatar = new \App\Avatar;
            }
            else{

                Storage::delete($avatar->relative_path);

            }
            $file  =request()->file('avatar');

            $directory = 'site-uploads/users_avatar';
            // $name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);// get name of file
            $name = strtolower(auth()->user()->first_name.'-'.auth()->user()->last_name);
            $fileName = $name.time().'.'.$file->extension();
            $half = $file->storeAs($directory,$fileName);
            $full = env('IMAGE_URL').'/storage/'.$half;

            if($avatar->createOrEdit($full,$half)){
                return apiResponse(1,'Avatar update', ['full_path'=> $full, 'relative_path' => $half]);
            }
            apiResponse(0,'An unexpected error occurred', []);
        }

        return apiResponse(0,'No file was sent', []);

    }


}
