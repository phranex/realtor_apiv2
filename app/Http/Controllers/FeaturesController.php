<?php

namespace App\Http\Controllers;

use App\Feature;
use Illuminate\Http\Request;

class FeaturesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $features = Feature::latest()->get();
        return view('admin.features.index', compact('features'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.features.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'type' => 'required'
        ]);

        $feature = new Feature;

        if($feature){
            $feature->createOrEdit($request->name, $request->description, $request->type);
            return redirect()->route('admin.features')->with('success', 'Feature created successfully');
        }



        return redirect()->route('admin.features');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $feature = Feature::find($id);

        return view('admin.features.edit', compact('feature'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'type' => 'required'
        ]);

        $feature = Feature::find($id);

        if($feature){
            $feature->createOrEdit($request->name, $request->description, $request->type);
            return redirect()->route('admin.features')->with('success', 'Feature updated successfully');
        }

        return back()->with('error', 'An unknown error occurred');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $feature = Feature::find($id);

        if($feature){
            $feature->delete();
            return back()->with('success', 'Feature removed successfully');
        }

        return back()->with('error', 'An unknown error occurred');

    }
}
