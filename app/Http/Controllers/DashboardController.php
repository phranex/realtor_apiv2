<?php

namespace App\Http\Controllers;

use App\Package;
use App\Payment;
use App\Review;
use App\Subscription;
use App\User;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //
    public function index()
    {
        # code...
        //get total users

        $total_users = User::all()->count();
        $total_reviews = Review::all()->count();
        $total_revenue =  Payment::getTotalRevenue();

        $total_subscriptions = Subscription::all()->count();
        $income_stat = Payment::getIncomeStat(5);
        // return Package::all();
        $paymentStat = Package::paymentStat();
        $users = User::latest()->take(10)->get();

        // return $rev_by_plan = Payment::getIncomePlanStat();


        /*

            get income stat for every month for the lst six month
        */




        return view('admin.dashboard.index', compact(
            'total_users',
            'total_reviews',
            'total_revenue',
            'total_subscriptions',
            'income_stat',
            'paymentStat',
            'users'
        ));
    }
}
