<?php

namespace App\Http\Controllers;

use App\Feature;
use App\Http\Resources\PackageResource;
use App\Package;
use App\Quality;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    //
    public function index()
    {
        //
        $packages = Package::with('qualities')->get();








        // $package = null;
        return view('admin.package.index', compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $features = Feature::all();
        return view('admin.package.create', compact('features'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //



        $request->validate([
            'name' => 'required|unique:packages,name',
            'price' => 'required',
            'features' => 'required',
            'feature_value' => 'required'
        ]);

        $package = new package;
        $features = request('features');
        $feature_values = request('feature_value');
        $feat = array_combine($features, $feature_values);
        // return $feat;
        if($package){
            $p = $package->createOrEdit($request->name, $request->price);
            if($p){
                foreach($feat as $key => $value){
                    $quality = Quality::where('package_id', $p->id)->where('feature_id',$feat[$key])->first();
                    if(empty($quality)){
                        $quality = new Quality;
                        //check if package and qualiy already in db

                        $quality->feature_id = $key;
                        $quality->package_id = $p->id;
                        if(is_numeric($value))
                            $quality->value = $value;
                        elseif($value === 'true')
                            $quality->value = 1;
                        else {
                            $quality->value = 0;
                        }


                        $quality->save();
                    }
                }
                return redirect()->route('admin.packages')->with('success', 'Package created successfully');
            }


        }



        return back()->withInput(request()->all())->with('error','An error occurred');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $package = Package::find($id);
        $pf = array_column($package->features->toArray(), 'id');
        $features = Feature::all();
        return view('admin.package.edit', compact('package','features', 'pf'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $request->validate([
            'name' => 'required|',
            'price' => 'required',
            'features' => 'required',
            'feature_value' => 'required'
        ]);

        $package = Package::find($id);
        $features = request('features');
        $feature_values = request('feature_value');
        $feat = array_combine($features, $feature_values);
        if($package){
            $p = $package->createOrEdit($request->name, $request->price);
            if($p){
                $p->removeQualities(); // remove old features
                foreach($feat as $key => $value){
                    $quality = Quality::where('package_id', $p->id)->where('feature_id',$feat[$key])->first();
                    if(empty($quality)){
                        $quality = new Quality;
                        //check if package and qualiy already in db

                        $quality->feature_id = $key;
                        $quality->package_id = $p->id;
                        if(is_numeric($value))
                            $quality->value = $value;
                        elseif($value === 'true')
                            $quality->value = 1;
                        else {
                            $quality->value = 0;
                        }


                        $quality->save();
                    }
                }
                return redirect()->route('admin.packages')->with('success', 'Package updated successfully');
            }

        }

        return back()->with('error', 'An unknown error occurred');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $package = Package::find($id);

        if($package){
            $package->remove();
            return back()->with('success', 'package removed successfully');
        }

        return back()->with('error', 'An unknown error occurred');

    }
}
