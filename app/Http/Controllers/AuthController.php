<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\User as UserResource;


use Illuminate\Http\Request;
use App\User;
use App\PersonalSetting;


class AuthController extends Controller
{
    //
       /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt', ['except' => ['login','register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Invalid Credentials'], 411);
        }

        return $this->respondWithToken($token);
    }

    public function  register(){
        $user = User::where(['email' => request('email')])->get();
        if(request('social')){
            if($user){
                $this->login();
            }
        }


        if(!count($user)){
            //validate request
            $form_request = request()->all();

            //return  response()->json(['status' => 0, 'message' => 'An unexpected error occurred','data' => $form_request]);

            if($this->validate_request($form_request)){

                $user = User::create([
                    'first_name' => $form_request['first_name'],
                    'last_name' => $form_request['last_name'],
                    'email' => $form_request['email'],
                    'password' => Hash::make($form_request['password']),
                ]);
                if($user){
                    //send email to user
                    $personal_setting = new PersonalSetting;
                    $biz = isset($form_request['business_name'])?$form_request['business_name']:'';
                    $personal_setting->createNew($user->id,$biz);
                    $user->createAsStripeCustomer();
                    return $this->login();
                }else{
                  return  response()->json(['status' => 0, 'message' => 'An unexpected error occurred','data' => []]);
                }
            }else{
                return  response()->json(['status' => 0, 'message' => 'An error occurred. Pleas make sure all fields are filled correctly','data' => []]);
            }

        }

        if(request('social')){
            return $this->login();
        }


        return response()->json([
            'status' => 0,
            'message' => __('Email already exists'),
            'data' => []
        ]);


    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = auth()->user();
        $userResource = new UserResource($user);
        return apiResponse(1,'success', $userResource);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }



    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'status' => 1,

            'message' => 'Login Successful',
            'data' => [
                'access_token' => $token,
                'token_type' => 'bearer',
                'user' => auth()->user(),
                'business_name' => @auth()->user()->setting->business_name,
                'expires_in' => auth()->factory()->getTTL() * 60 * 60 * 60 * 1200,
                'identifier' => createIdentifier(),
                'subscription' => auth()->user()->paymentSubscription()
            ],

        ]);
    }

    public function validate_request($post_request)
    {
        # code...
        if(count($post_request)){
            foreach($post_request as $item){
                if(!isset($item))
                    return false;
            }
            return true;
        }
        return false;

    }

    public function changePassword(Request $request)
    {
        if(auth()->user()->blocked)
        return apiResponse(0, 'Blocked', []);

         $form_request = request()->all();
         if($this->validate_request($form_request)){
            $old = $form_request['old_password'];
            $user = \App\User::find(auth()->id());
            if($user){
                if(Hash::check($old,$user->password)){
                    $user->password = Hash::make($form_request['password']);
                    $user->save();
                    auth()->logout();
                    return apiResponse(1, 'success', []);
                }else{
                    return apiResponse(56, trans('Your password is incorrect'), []);
                    // return back()->with('error', 'Your password is incorrect');
                }
            }
            return apiResponse(0, trans('An enexpected error occurred'), []);

         }


    }
}
