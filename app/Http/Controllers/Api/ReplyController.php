<?php

namespace App\Http\Controllers\Api;

use App\Reply;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        //
        if(!is_numeric($id)) return apiResponse(0,'An unexpected error occurred', []);

        $form_request = request()->all();
        if(isset($form_request['reply'])){
            //get review
            $reply_to_review = $form_request['reply'];
            $review = \App\Review::find($id);
            if($review){
                if(isset($review->contact_id))
                    $email = $review->contact->email;
                elseif(isset($review->email))
                    $email = $review->email;
                else $email = null;
                $reply = new \App\Reply;
                $reply->createOrEdit($review->id,$reply_to_review);
                if($email != null){
                    //send mail to the reviewer
                    sendReplyToReviewer($email,$review,$reply);
                    return apiResponse(1, 'success', []);
                }else
                    return apiResponse(0,'Reviewer didn\'t drop an email. Can not send a reply',[]);
            }
            return apiResponse(0,'Review doesnt exist', []);
        }else{
            return apiResponse(0,'No reply was sent', []);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function show(Reply $reply)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function edit(Reply $reply)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reply $reply)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reply $reply)
    {
        //
    }


}
