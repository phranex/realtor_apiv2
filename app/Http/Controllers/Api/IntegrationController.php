<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Http\Resources\IntegrationResource;
use \App\Integration;


class IntegrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        if(auth()->user()->thirdPartyIntegrations){
            $integrations = IntegrationResource::collection(auth()->user()->thirdPartyIntegrations);
            $identifier = createIdentifier(); //id-fullname-;
            return apiResponse(1,'success', ['integrations'  => $integrations, 'identifier' => $identifier]);
        }

        $identifier = auth()->id().'-'.auth()->user()->first_name.'_'.auth()->user()->last_name; //id-fullname-;
        return apiResponse(1,'success', ['integrations'  => auth()->user()->thirdPartyIntegrations, 'identifier' => base64_encode($identifier)]);



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


        $form =request()->all();
        $num = count($form);
        $saved = 0;
        if($num){
            foreach ($form as $data) {
                # code...
                 $required = [
                        'access_token',
                        'external_id',
                        'third_party_integration_id',



                    ];
                if(validate_request($data,$required)){
                    logger($data['external_id']);
                    $integration = Integration::where('external_id',$data['external_id'])->where('user_id', auth()->id())->first();

                    if(!$integration){
                        logger('inside');
                        $integration = new Integration;
                        if($integration->create((object) $data)){
                            $saved++;

                        }

                    }
                    // return apiResponse(0,__('Integration already set'), []);
                }else{
                    logger('failed');
                }

            }
            if($saved == 0){
                return apiResponse(0,__('Couldnt integrate. Please try again'), []);
            }

            if($saved == $num){
                return apiResponse(1,__('Success'), []);
            }else{
                return apiResponse(0,__('Some couldn\'t  be integrated successfully'), []);
            }


        }


        return apiResponse(0,__('Please fill all fields correctly'), []);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public  function getTypes(){

        // $integrations = IntegrationResource::collection(\App\ThirdPartyIntegration::all());

        return apiResponse(1,'success',\App\ThirdPartyIntegration::all());

    }
}
