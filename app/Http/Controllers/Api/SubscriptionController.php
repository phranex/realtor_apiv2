<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subscription;

class SubscriptionController extends Controller
{
    //
    public function store(Request $request)
    {
        //
        if(!empty(request('email')) && filter_var(request('email'), FILTER_VALIDATE_EMAIL)){
            $subscription = Subscription::where('email', request('email'))->first();
            if(empty($subscription)){
                $subscription = new Subscription;
                if($subscription->create(request('email'))){
                    return apiResponse(1, 'success', []);
                }else{
                    return apiResponse(0,__('An error occurred'), []);
                }
            }
            return apiResponse(56,__('Email already sent'), []);

        }

        return apiResponse(56, 'Invalid field', []);
    }
}
