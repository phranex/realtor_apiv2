<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\User as UserResource;
use \App\PersonalSetting;
use \App\User;
use App\Http\Resources\ProfileSettingResource;
use App\Review;
use DB;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //


        $contacts = [
            'total' => count(auth()->user()->contacts),
        ];
        $reviews = [
            'total' => count(auth()->user()->reviews)
        ];
        //get reviews where date is between now and last six month. group by month.
        $now = \Carbon\Carbon::now();
        $months = [];
        for($i = 5; $i >= 0; $i--){
            $month = \Carbon\Carbon::now()->subMonths($i)->format('M Y');
            array_push($months, $month);
        }
        $from = \Carbon\Carbon::now()->subMonths(5)->startOfMonth();
        $reviews_count = Review::where('user_id', auth()->id())
                                ->whereBetween(DB::raw('DATE(created_at)'), array($from, $now))
                                ->orderBy('created_at','asc')
                                ->selectRaw('COUNT(DATE_FORMAT(created_at, "%b")) as count, DATE_FORMAT(created_at, "%b %Y") AS month_year')
                                ->groupBy('created_at')
                                ->get()
                                ->toArray();
        $x = array_column((array) $reviews_count, 'month_year');
        $y =  $reviews_count;
        for($i = 0; $i < count($months); $i++){

            if($index = array_search($months[$i], $x) !== false){
                $m[$months[$i]] = $y[$index]['count'];
            }else{
                $m[$months[$i]] = 0;
            }
        }
        $result = [
            'contacts' => $contacts,
            'reviews' => $reviews,
            'avg_rating' => auth()->user()->getUserAverageReview(),
            'reviews_count' =>$m,
            'subscription' => auth()->user()->paymentSubscription()


        ];


        return apiResponse(1,'success', $result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($business_name)
    {
        //
        $personalSetting = PersonalSetting::where('business_name',$business_name)->first();
        if($personalSetting){
            $user = User::where('id',$personalSetting->user_id)->first();
            $userResource = new UserResource($user);
            return apiResponse(1,'success', $userResource);
        }
        return apiResponse(0,'User Not Found', []);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function searchByBusinessName()
    {
        # code...
        if(!empty(request('query'))){
            $business_name = request('query');
            // $business_name = str_replace(' ','_',$business_name);

            $users = User::where('first_name','LIKE', "%$business_name%")->orWhere('last_name','LIKE', "%$business_name%")->get();
            logger($users);
            if(!count($users)){
                $keywords = str_split($business_name,4);
                $users = User::where(function($query) use ($keywords){
                        foreach($keywords as $keyword) {
                            $query->orWhere('first_name', 'LIKE', "%$keyword%");
                            $query->orWhere('last_name', 'LIKE', "%$keyword%");

                        }
                    })->get();
            }

            // if(!empty($setting)){
            //     $setting = User::where(function($query) use ($keywords){
            //         foreach($keywords as $keyword) {
            //             $query->orWhere('first_name', 'LIKE', "%$keyword%");
            //             $query->orWhere('last_name', 'LIKE', "%$keyword%");

            //         }
            //     })->get();

            // }

            if($users){
                $users = UserResource::collection($users);
                return apiresponse(1,'Users found',$users);
            }
            return apiresponse(1,'User Not found',[]);
        }


        return apiResponse(0,'error', []);
    }


}
