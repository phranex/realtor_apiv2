<?php

namespace App\Http\Controllers\Api;

use App\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Http\Resources\ReviewResource;
use App\Http\Resources\User;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(!empty(request('limit')) && request('limit') == 0){
            $limit = request('limit');
            $reviews =  ReviewResource::collection(\App\Review::latest()->take($limit));
        }else
            $reviews =  ReviewResource::collection(\App\Review::all());
        if($reviews)
            return apiResponse(1,__('success'),$reviews);

         return apiResponse(0,__('Error'),[]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $required = [
            'title',
            'review',
            'rating_score',
            'user_id',
            'invitation_id'
        ];


        // if(!request('anonymous'))
        //     $required[] = 'invitation_id';
        $form =request()->all();
        logger($form);

        if(validate_request($form,$required)){
            //return true;
            $review = Review::where('invitation_id',$request->invitation_id)->where('user_id', $request->user_id)
            ->orWhere('email',$request->email)->where('user_id', $request->user_id)->first();

            // if(!request('anonymous')){
            //     $review = Review::where('invitation_id',$request->invitation_id)->where('user_id', $request->user_id)
            //     ->orWhere('email',$request->email)->where('user_id', $request->user_id)->first();

            // }
            // else {
            //     $review = Review::where('email',$request->email)->where('user_id', $request->user_id)->first();

            // }
            if(empty($review))
                $review = new Review;
            else {
                return apiResponse(0,__('Review already submitted'), []);
            }
            if($review->create($request)){
                $invitation = \App\Invitation::find($request->invitation_id);
                if($invitation){
                    $invitation->status = 1;
                    $invitation->save();
                }
                return apiResponse(1,__('Review Submitted'), []);
            }
            return apiResponse(0,__('An error occurred'), []);
        }

        return apiResponse(0,__('Please fill all fields correctly'), []);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function show(Review $review)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Review $review)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review)
    {
        //
    }
    public function getRealtorReviews()
    {
        # code...
        if(!empty(request('identifier'))){
            $user = base64_decode(request('identifier'));
            logger(request('identifier'));
            $user = explode('-',$user);
            $user = \App\User::find($user[0]);
            if(!$user) return apiResponse(0, 'User Not Found', []);
            else $id = $user->id;
        }else{
            $id = auth()->id();
        }



        $r = Review::where('user_id',$id)->groupBy('rating_score')->selectRaw('rating_score,count(rating_score) as count')->OrderBy('rating_score', 'desc')->get();


        //get total avg
        $rev = Review::where('user_id', $id)->pluck('rating_score')->sum();
        $limit = 1;
        if(!empty(request('page'))) $current = request('page') - 1;
        else $current = 0;
        $next_page = $current * $limit;
        $total = count(Review::where('user_id', $id)->get());
        $reviews = ReviewResource::collection(Review::where('user_id', $id)->take($limit)->skip($next_page)->get());
        $data = [
            'reviews' => $reviews,
            'pagination' => [
                'total' => $total,
                'current_page' => $current,
                'previous_page' => request('page')?request('page') - 1:0,
                'next_page' => $current + 1,
                'limit' => $limit
            ],
            'ratings' => [
                'rating_scores_count' => $r,
                'avg_rating' => $total > 0? round($rev / $total): 0,
                'total' => $total
            ]
        ];
        return apiResponse(1, 'success', $data);
    }
}
