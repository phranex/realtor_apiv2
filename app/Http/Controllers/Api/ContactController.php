<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use  \App\Contact;
use \App\Http\Resources\ContactResource;
use App\Exports\ContactsExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Invitation;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $contacts =  ContactResource::collection(auth()->user()->contacts);
        if($contacts)
            return apiResponse(1,__('success'),$contacts);

         return apiResponse(0,__('Error'),[]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $required = [
            'fullname','email'
        ];
        $form =request()->all();

        if(validate_request($form,$required)){
            if(isset($form['contact_id'])){
                $contact = Contact::find($form['contact_id']);
            }
            $contact = Contact::where('fullname', $form['fullname'])->where('email', $form['email'])->first();
            if(!isset($contact))
                $contact = new Contact;
            if($contact->createOrEdit($request)){
                return apiResponse(1,__('Contact Updated'),[]);
            }
            return apiResponse(0,__('Contact could not be added/updated'),[]);
        }

        return apiResponse(0,__('Please fill all fields correctly'), []);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $required = [
            'fullname','email'
        ];
        $form =request()->all();

        if(validate_request($form,$required)){
            if(isset($form['contact_id'])){
                $contact = Contact::find($form['contact_id']);
                if($contact->createOrEdit($request)){
                    return apiResponse(1,__('Contact Updated'),[]);
                }
                return apiResponse(0,__('Contact could not be updated'),[]);
            }
            return apiResponse(0,__('An error occurred'), []);

        }

        return apiResponse(0,__('Please fill all fields correctly'), []);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $contact = Contact::find($id);
        if(isset($contact)){
            if(auth()->id() == $contact->user->id){
                $contact->delete();
                return apiResponse(1,'Contact Deleted',[]);
            }
            return apiResponse(0,'Unauthorized',[]);
        }
        return apiResponse(0,'AN error ocurred',[]);
    }

    public function getBulkFormat()
    {
        # code...
       $contact = new Contact; // contacts table

        $columns = $contact->getTableColumns(); // dump the result and die
        $hide = ['id', 'user_id', 'created_at','updated_at'];
        for($i = 0; $i < count($hide); $i++){
            if (($key = array_search($hide[$i], $columns)) !== false) {
                unset($columns[$key]);
            }
        }
        $contact = Excel::download(new ContactsExport, 'contacts.xlsx');
        return apiResponse(1,'success',[$contact]);

        return $columns;
    }

    public function search()
    {
        # code...
        if(!empty(request('query'))){
            $quer = request('query');

            // check query if email
            $is_email = filter_var($quer, FILTER_VALIDATE_EMAIL);
            if($is_email) $column = 'email'; else $column = 'fullname';
            $contacts = Contact::where('user_id', auth()->id())->where($column,'LIKE',"%$quer%")->get();
            if(!count($contacts)){
                logger('logg');
                $q = substr($quer,0,30);
                $keywords = str_split($q,4);
                $contacts = Contact::where('user_id', auth()->id())->where(function($query) use ($keywords,$column){
                        foreach($keywords as $keyword) {
                            $query->orWhere($column, 'LIKE', "%$keyword%");
                        }
                    })->get();
            }
            logger($contacts);
            return apiResponse(1,'success', $contacts);
        }

        return apiResponse(0, 'Please enter a search query', []);
    }

    public function filter()
    {
        # code...
        if(!empty(request('filter'))){
            $filter = request('filter');
            switch ($filter) {
                case 1://request sent
                    # code...
                    $invitations =  Invitation::with('contact')->where('user_id', auth()->id())->where('contact_id', '!=', null)->groupBy('contact_id')->get();
                    $contacts = [];
                    foreach ($invitations as $invitation) {
                        # code...
                        array_push($contacts, $invitation->contact);
                    }
                    break;

                case 2://request not sent
                    # code...
                    $invitations =  Invitation::where('user_id', auth()->id())->where('contact_id', '!=', null)->pluck('contact_id');
                    $contacts = Contact::where('user_id', auth()->id())->whereNotIn('id' ,$invitations)->get();
                    break;
                case 3://request responded to
                    # code...
                    $invitations =  Invitation::with('contact')->where('user_id', auth()->id())->where('status', 1)->get();
                    $contacts = [];
                    foreach ($invitations as $invitation) {
                        # code...
                        array_push($contacts, $invitation->contact);
                    }
                    break;
                case 4://request not responded to
                    # code...
                    $invitations =  Invitation::with('contact')->where('user_id', auth()->id())->where('status', 0)->groupBy('contact_id')->get();
                    $contacts = [];
                    foreach ($invitations as $invitation) {
                        # code...
                        array_push($contacts, $invitation->contact);
                    }
                    break;


                default:
                    # code...
                    return apiResponse(0, 'Error occurred', []);

                    break;
            }


            // check query if email
            return apiResponse(1, 'Please enter a search query', $contacts);

        }


    }
}
