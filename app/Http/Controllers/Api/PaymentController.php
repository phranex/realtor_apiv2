<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Package;
use App\Payment;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(!empty($request->payment_method) && !empty($request->plan) && !empty($request->plan_id)){
            $user = auth()->user();
            $plan = $request->plan;
            $plan_id = $request->plan_id;
            //get package from db
            $package = Package::where('id', $request->plan_id)->where('name', $request->plan)->first();

            if($package){
                $payment = new  Payment;
                $plan = strtolower($package->name);
                // return apiResponse(1, 'Payment added', $user_payment_method);
                if(!$user->hasPaymentMethod()){
                    //create a new payment method
                    //create a subscription
                    // $payment_card = $user->addPaymentMethod($request->payment_method);
                    $sub = $user->newSubscription($plan, $plan)->create($request->payment_method);

                    $payment->createPayment($sub->id,$package->price);
                }else{
                    //use default card
                    $payment_card =  $user->defaultPaymentMethod();

                   $sub = $user->newSubscription($plan, $plan)->create($payment_card);
                   $payment->createPayment($sub->id,$package->price);

                }




                //send invoice mail notification about subscription to user
                // sendInvoicemail();
                return apiResponse(1, 'subscribed', []);
            }
            return apiResponse(0, 'Plan does not exist', []);
        }

        return apiResponse(0, 'Missing parameter', []);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
