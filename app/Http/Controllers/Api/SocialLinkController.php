<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\SocialLinkType;
use \App\SocialLink;

class SocialLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $form_request = request()->all();
        // return response()->json([
        //     'status' => 1,
        //     'message' => 'here',
        //     'data' => $form_request
        // ]);
        if(count($form_request)){
            foreach ($form_request as $key => $value) {
                # code...
                $socialType = SocialLinkType::where('name', $key)->first();
                if($socialType){
                    //check if social already esist
                    $socialLink = SocialLink::where('socialLinkType_id',$socialType->id)->where('user_id',auth()->id())->first();
                    if(!isset($socialLink))
                        $socialLink = new SocialLink;
                    $socialLink->createOrEdit($socialType->id,$value);
                }
                continue;
            }
            return apiResponse(1,'Social Links Updated',[]);
        }

        return apiResponse(0,'An error occurred',[]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

    }
}
