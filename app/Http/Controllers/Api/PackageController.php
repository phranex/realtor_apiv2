<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PackageResource;
use App\Package;

class PackageController extends Controller
{
    //
    public function index()
    {
        # code...
        $packages = Package::with('qualities.feature')->get();

        // $packages = PackageResource::collection($packages);

        return apiResponse(1, 'success', $packages);

    }

    public function show($id){
        $packages = Package::with('qualities.feature')->where('id', $id)->first();
        if(empty($packages))   return apiResponse(0, 'Error: not found', []);
        // $packages = PackageResource::collection($packages);

        return apiResponse(1, 'success', $packages);
    }
}
