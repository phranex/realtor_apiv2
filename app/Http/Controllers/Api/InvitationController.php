<?php

namespace App\Http\Controllers\Api;

use App\Invitation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\Http\Resources\InvitationResources;
use App\Contact;
use App\Template;

class InvitationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // if(!count(auth()->user()->contacts)){
        //     return apiResponse(1,'success', ['num_of_contacts' => count(auth()->user()->contacts)]);
        // }
        $invitations = InvitationResources::collection((auth()->user()->invitations));
        if($invitations)
        return apiResponse(1,'success', ['invites' => auth()->user()->invitations,'num_of_contacts' => count(auth()->user()->contacts) ]);



        return apiResponse(0,'error', []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $required = [
            'message'
        ];
        $form =request()->all();

        // if(validate_request($form,$required)){
        //     $message = $form['message'];
        //     if($form['all']){
        //         ///get all users contacts
        //         $contacts_emails = auth()->user()->contacts;

        //         $contact_ = true;
        //     }elseif(count($form['emails'])){
        //         $contacts_emails = $form['emails'];
        //     }

        //     if(!empty($contacts_emails)){
        //         $mails_not_sent = [];

        //         foreach($contacts_emails as $contact){
        //             $invitation = new Invitation;

        //             if(isset($contact_)){
        //                 $email = $contact->email;

        //                 $code = base64_encode($email.'_'.time());

        //                 $contact_id = $contact->id;
        //                 $saved = $invitation->create($email,$code,$contact_id);
        //             }else{
        //                 $email = $contact;
        //                 $code = base64_encode($email.'_'.time());
        //                 $saved = $invitation->create($email,$code);
        //             }

        //             if($saved){
        //                 $url = env('FRONT_END_BASE_URL').'user/invitations/show/'.$code;
        //                 if(sendInvitation($contact,$message,$url)){
        //                     $saved->mail_sent = 1;
        //                     $saved->save();
        //                 }
        //                 //array_push($mails_not_sent, $contact_email);
        //                 ///continue;
        //             }


        //         }
        //         return apiResponse(1,__('Invitations successfully sent'), []);
        //     }
        // }

        if(validate_request($form,$required)){

            $message = $form['message'];
            if($form['new']){
                $name = $form['template_name'];
                ///get all users contacts
                //create new template
                $template = new Template;
                $template->createOrEdit($name,$message);
            }
            if(count($form['emails'])){
                $contacts_id = $form['emails'];
            }

            if(!empty($contacts_id)){
                $mails_not_sent = [];

                foreach($contacts_id as $id){
                    $contact = Contact::find($id);
                    if($contact){
                        $invitation = new Invitation;
                        $email = $contact->email;
                        $code = base64_encode($email.'_'.time());
                        $contact_id = $contact->id;
                        $saved = $invitation->create($email,$code,$contact_id);
                    }

                    if($saved){
                        $url = env('FRONT_END_BASE_URL').'user/invitations/show/'.$code;
                        if(sendInvitation($contact,$message,$url)){
                            $saved->mail_sent = 1;
                            $saved->save();
                        }
                        //array_push($mails_not_sent, $contact_email);
                        ///continue;
                    }


                }
                return apiResponse(1,__('Invitations successfully sent'), []);
            }
        }
        return apiResponse(0,__('Please fill all fields correctly'), []);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invitation  $invitation
     * @return \Illuminate\Http\Response
     */
    public function show(Invitation $invitation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invitation  $invitation
     * @return \Illuminate\Http\Response
     */
    public function edit(Invitation $invitation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invitation  $invitation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invitation $invitation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invitation  $invitation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invitation $invitation)
    {
        //
    }

    public function showByCode($code)
    {
        # code...
        if(!empty($code)){
            $invitation = Invitation::where('invitation_code',$code)->first();
            logger($invitation);
            if(!is_null($invitation)){
                //check if
                $data = new InvitationResources($invitation);
                if($data->status == 1) return apiResponse(56, 'You have already submitted a review', []);
                return apiResponse(1, 'success', $data);
            }else{
                return apiResponse(56, 'Not found', []);
            }
        }
        return apiResponse(0, 'error', []);

    }
}
