<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $settings = \App\Setting::all();
        $contents = [];
        $banners = [];
        if(count($settings)){


            foreach($settings as $setting){
                if($setting->name != 'enabler'){
                    $contents[$setting->name] = unserialize($setting->value);
                }

            }
            $banners_ =  \App\Setting::where('name','review')->get();
            $banners = [];
            if($banners_){
                foreach ($banners_ as  $banner) {

                    $ban['id'] = $banner->id;
                    $ban['content'] = unserialize($banner->value);
                    array_push($banners, $ban);
                }
            }


            // foreach($settings as $setting){
            //     if(array_key_exists($setting->name, $contents)){
            //         if(gettype($contents[$setting->name]) == 'string'){
            //             $value = $contents[$setting->name];
            //             $contents[$setting->name] = [];
            //             array_push($contents[$setting->name],$value);
            //             array_push($contents[$setting->name],unserialize($setting->value));
            //         }else{
            //             array_push($contents[$setting->name],unserialize($setting->value));
            //         }
            //     }else{
            //         $contents[$setting->name] = unserialize($setting->value);

            //     }
            // }

        }
        return view('admin.setting.index',compact('contents','banners'));

    }

    public function pageSetup($page)
    {
        # code...
        if(!empty($page)){

            $setting = \App\Setting::where('name', $page)->first();

            if($setting){
                $setting = unserialize($setting->value);
                // $products = [];
                // $new_products = [];
                // $featured_products = [];



                $result = [
                    'setting' => $setting,

                ];
                return apiResponse(1, 'success', $result );
            }

            return apiResponse(56, 'Setting not found', [] );

        }

        return apiResponse(0,'error', []);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function indexPage(Request $request)
    {
        # code...
        $request->validate([
            'banner-title-lg' => 'required',
            'banner-title-sm' => 'required',
            'realtor-banner-title-lg' => 'required',
            'site-description' => 'required',
            'pricing-description' => 'required',
            'realtor-banner-title-sm' => 'required',
            'realtor-banner-title-sm' => 'required',
            'realtor-banner-title-sm' => 'required',

            // 'how-it-works-step-3' => 'required',
            // 'how-it-works-step-3' => 'required',




        ]);

        foreach ($request->except('_token') as $key => $value) {
            $data[$key] = $value;
        }
        $data = serialize($data);
        $settings = new Setting;
        $name = 'index';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','Index Page successfully updated');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','Index Page successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');
        // /print($s);

    }

    public function how_it_works_page(Request $request)
    {

        # code...
        $request->validate([
            'how-it-works-subtitle' => 'required',
            'how-it-works-description' => 'required',
            'how-it-works-step1' => 'required',
            'how-it-works-step2' => 'required',
            'how-it-works-step3' => 'required',



        ]);

        foreach ($request->except('_token') as $key => $value) {
            $data[$key] = $value;
        }

        $data = serialize($data);
        $settings = new Setting;
        $name = 'hiw';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','How it works Page successfully updated');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','How it works Page successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');
        // /print($s);

    }


    public function instruction(Request $request)
    {
        # code...

        $request->validate([
            'editordata' => 'required',

        ]);


        foreach ($request->except('_token') as $key => $value) {
            $data[$key] = $value;
        }
        $data = serialize($data);
        $settings = new Setting;
        $name = 'instruction';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','Instruction successfully created');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','Instructions successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');


    }

    public function payments(Request $request)
    {
        # code...


        $request->validate([
            'editordata' => 'required',

        ]);


        foreach ($request->except('_token') as $key => $value) {
            $data[$key] = $value;
        }
        $data = serialize($data);
        $settings = new Setting;
        $name = 'payments';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','Instruction successfully created');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','Automatic Payments successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');


    }

    public function terms(Request $request)
    {
        # code...

        $request->validate([
            'editordata' => 'required',

        ]);


        foreach ($request->except('_token') as $key => $value) {
            $data[$key] = $value;
        }
        $data = serialize($data);
        $settings = new Setting;
        $name = 'terms';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','Terms of servcice successfully created');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','Terms of service successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');


    }

    public function personal(Request $request)
    {
        # code...

        $request->validate([
            'editordata' => 'required',

        ]);


        foreach ($request->except('_token') as $key => $value) {
            $data[$key] = $value;
        }
        $data = serialize($data);
        $settings = new Setting;
        $name = 'policy';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','Privicy policies successfully created');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','Privacy Policies successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');


    }

    public function guarantee(Request $request)
    {
        # code...

        $request->validate([
            'editordata' => 'required',

        ]);


        foreach ($request->except('_token') as $key => $value) {
            $data[$key] = $value;
        }
        $data = serialize($data);
        $settings = new Setting;
        $name = 'guarantee';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','Instruction successfully created');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','Instructions successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');


    }

    public function contact_page(Request $request)
    {
        # code...
        $request->validate([

            'phone-number' => 'required',
            'email' => 'required',



        ]);


        foreach ($request->except('_token') as $key => $value) {
            $data[$key] = $value;
        }
        $data = serialize($data);
        $settings = new Setting;
        $name = 'contact';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','Contact Us Page successfully updated');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','Contact Us Page successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');


    }


    public function faq_page(Request $request)
    {
        # code...
        $index = 0;
        $loop = 1;

        foreach ($request->except('_token') as $key => $value) {
            if($loop%2){
                $questions[$loop] = $value;
            }else{
                $answers[$loop] = $value;
            }
            $loop++;
        }
        $result = array_combine($questions,$answers);

        $data = serialize($result);


        $settings = new Setting;
        $name = 'faq';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','FAQ Page successfully updated');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','FAQ Page successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');


    }

     public function features(Request $request)
    {
        # code...
        $index = 0;
        $loop = 1;
        $para = $request->except('_token');
        if(!count($para)) return back();

        foreach ($request->except('_token') as $key => $value) {
            if($loop%2){
                $questions[$loop] = $value;
            }else{
                $answers[$loop] = $value;
            }
            $loop++;
        }
        $result = array_combine($questions,$answers);

        $data = serialize($result);


        $settings = new Setting;
        $name = 'features';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','Features successfully updated');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','Features successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');


    }
    public function hiw_page(Request $request)
    {
        # code...
        $index = 0;
        $loop = 1;

        foreach ($request->except('_token') as $key => $value) {
            if($loop%2){
                $questions[$loop] = $value;
            }else{
                $answers[$loop] = $value;
            }
            $loop++;
        }
        $result = array_combine($questions,$answers);

        $data = serialize($result);


        $settings = new Setting;
        $name = 'about';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','ABout Page successfully updated');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','About Page successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');


    }
    public function uploadBanner(Request $request)
    {
        # code...
        // return request()->all();
        $captions = $request->except(['photo','_token']);

        $request->validate([
            'photo[].*' => 'required|image|array|max:2000'
        ]);
        $num = 0;
        $paths = [];
        $banners = [];
        $index = 0;

        if(request()->hasFile('photo')){
            if(count(request()->file('photo'))){
                foreach(request()->file('photo') as $file){
                    if($file){
                        $directory = 'uploads/reviews';
                        $name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);// get name of file

                            //dd($name);
                        $fileName = "review_".$name.'.'.$file->extension();
                        $path = $file->storeAs($directory,$fileName);
                        array_push($paths,getImageLink($path));

                    }else {
                        continue;
                    }


                }

                $banner_index = 0;
                $banner_items = $request->except(['_token','photo']);
                $banner_items = array_values($banner_items);

                for( $k = 0; $k < count($paths); $k++){
                    $num  = $k * 2;


                    $banner = [];
                    $banner['review'] = $banner_items[$num];
                    $banner['path'] = $paths[$k];
                    $banner['name'] =  $banner_items[$num+1];
                    array_push($banners,$banner);
                }

                foreach($banners as $banner){
                    $setting = new Setting;
                    $path = serialize($banner);
                    $name = 'review';
                    $setting->create($name, $path);
                }



                return back()->with('success', 'Review uploaded successfully');
            }

        }

       return back()->with('error', 'Review could not be saved');
    }

    public function deleteBanner($banner)
    {
        # code...

        if(is_numeric($banner)){
            $banner = Setting::find($banner);

            if($banner){
                //$banner = unserialize($banner);
                $ban = unserialize($banner->value);
                $path = env('IMAGE_URL').'/storage/';
                $banne = str_replace($path,'',$ban);
                Storage::delete($banne['path']);
                $banner->delete();
                return back()->with('success', 'Review deleted successfully');
            }
        }
        return back()->with('error', 'Reviuew could not be deleted');
    }


}
