<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Collections\SocialLinkCollection;
use App\Http\Resources\SocialLink;
use App\Http\Resources\ContactResource;
use App\Http\Resources\ProfileSettingResource;
use App\Http\Resources\ReviewResource;



class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
                'id' => $this->id,
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
                'fullname' => $this->first_name . ' '. $this->last_name,
                'avatar' => @$this->avatar['full_path'],
                'email' => $this->email,
                'profile' =>  new ProfileSettingResource($this->setting),
                'invitations' => $this->invitations,
                'reviews' => isset($this->reviews)?ReviewResource::collection($this->reviews):0,
                'social' => SocialLink::collection($this->socialLinks),
                'contacts' => ContactResource::collection($this->contacts),
                // 'subscription' => $this->paymentSubscription()

        ];
    }


}
