<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\ReviewResource;


class ProfileSettingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'business_name' => str_replace('_', ' ', $this->business_name),
            'title' => $this->title,
            'phone_number' => $this->phone_number,
            'email' => $this->email,
            'address' => $this->address,
            'address2' => $this->address2,
            'date_of_birth' => $this->date_of_birth,
            'city' => $this->city,
            'state' => $this->state,
            'country' => $this->country,
            'zip' => $this->zip,
            'user' => [
                'data' => $this->user,
                'avatar' => @$this->user->avatar
            ],
            // 'num_of_reviews' => count($this->user->reviews),
            'reviews' => [
                'count' => isset($this->user->reviews)?count($this->user->reviews):0,
                'average_rating' => isset($this->user->reviews)?$this->user->getUserAverageReview():0,
                'data' => isset($this->user->reviews)?$this->user->reviews:0,

            ]
        ];
    }
}
