<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InvitationResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'code' => $this->invitation_code,
            'email' => $this->email,
            'contact' => $this->contact,
            'user' => $this->user,

            'status' => $this->status,
            'business_name' => $this->user->setting->business_name,
            'review' => $this->review,
            'total_contacts' => @auth()->user()->contacts? count(auth()->user()->contacts):null
        ];
    }
}
