<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SocialLink extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            
                'id' => $this->id,
                'user_id' => $this->user_id,
                'socialLinkType_id' => $this->socialLinkType_id,
                'link' => $this->link,
                'socialLinkType' => [
                    'name' => $this->socialLinkType->name,
                    'isRequired' => $this->socialLinkType->isRequired
                ]
        
            
            ];
    }
}
