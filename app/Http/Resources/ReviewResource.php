<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'user' => $this->user,
            'business_name_slug' => @$this->user->setting->business_name,
            'business_name' => str_replace('_', ' ',$this->user->setting->business_name),
            'title' => $this->title,
            'review' => $this->review,
            'email' => $this->email,
            'fullname' => $this->fullname,
            'id' => $this->id,
            'replies' => $this->replies,
            'rating_score' => $this->rating_score,
            'contact' => $this->contact,
            'created_at' => $this->created_at,
            'when' => \Carbon\Carbon::parse($this->created_at)->diffForHumans(),

        ];
    }


}
