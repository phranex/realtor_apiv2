<?php

namespace App\Http\Resources\Collections;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SocialLinkCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'status' => 1,
            'message' => 'success',
            'data'  => [
                'id' => $this->id,
                'user_id' => $this->user_id,
                'socialLinkType_id' => $this->socialLinkType_id,
                'link' => $this->link,
                'socialLinkType' => $this->socialLinkType->name
        
            ]
            ];
    }
}
