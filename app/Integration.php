<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Integration extends Model
{
    //
    public function create($request)
    {
        # code...
        $this->user_id = auth()->id();
        $this->third_party_integration_id = $request->third_party_integration_id;
        $this->external_id = $request->external_id;
        $this->access_token = $request->access_token;
        $this->external_name = $request->name;
        $this->save();
        return $this;
    }

    public function thirdPartyIntegration()
    {
        return $this->belongsTo('App\ThirdPartyIntegration');
    }
}
