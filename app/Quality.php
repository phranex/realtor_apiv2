<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quality extends Model
{
    //

    public function feature()
    {
        return $this->belongsTo('App\Feature', 'feature_id');
    }

    public function package()
    {
        return $this->belongsTo('App\Package', 'package_id');
    }
}
