<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    //

    public function createOrEdit($name,$description, $type)
    {
        # code...
        $this->name = $name;
        $this->description = $description;
        $this->type = $type;
        return $this->save();
    }



}
