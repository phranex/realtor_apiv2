<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    //
    public function create($email,$code,$contact_id = null)
    {
        # code...
        $this->user_id = auth()->id();
        $this->contact_id = $contact_id;
        $this->email = $email;
        $this->invitation_code = $code;
        $this->save();
        return $this;
    }

    public function contact()
    {
        return $this->belongsTo('App\Contact', 'contact_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function review()
    {
        return $this->hasOne('App\Review', 'invitation_id');
    }
}
