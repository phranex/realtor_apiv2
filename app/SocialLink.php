<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialLink extends Model
{
    //
    public function createOrEdit($id,$link)
    {
        # code...
        $this->user_id = auth()->id();
        $this->socialLinkType_id = $id;
        $this->link = $link;
        return $this->save();
    }

   
    public function socialLinkType()
    {
        return $this->belongsTo('App\SocialLinkType', 'socialLinkType_id');
    }
    
   
}
