<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    //
    public function create($request)
    {
        # code...
        $this->user_id = $request->user_id;
        $this->contact_id = $request->contact_id;
        $this->invitation_id = $request->invitation_id;
        $this->email = $request->email;
        $this->fullname = $request->fullname;
        $this->title = $request->title;
        $this->review = $request->review;
        $this->rating_score = $request->rating_score;
        return $this->save();
    }

    public function contact()
    {
        return $this->belongsTo('App\Contact');
    }

    public function replies()
    {
        return $this->hasMany('App\Reply');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function invitation()
    {
        return $this->belongsTo('App\Invitation');
    }
}
