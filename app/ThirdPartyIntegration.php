<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThirdPartyIntegration extends Model
{
    //
    public function create($request)
    {
        # code...
        $this->name = $request->name;
        $this->title = $request->title;
        $this->color = @$request->color;
        return $this->save();
    }
}
