<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //

    public function createOrEdit($request)
    {
        # code...
        $this->user_id = auth()->id();
        $this->fullname = $request->fullname;
        $this->email = $request->email;
        $this->phone_number = $request->phone_number;
        return $this->save();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }
}
