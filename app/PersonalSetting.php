<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalSetting extends Model
{
    //
    public function createOrEdit($request)
    {
        # code...
        $this->title = $request['title'];
        $this->business_name = str_replace(' ', '_',$request['business_name']);
        $this->phone_number = $request['phone_number'];
        $this->address = $request['address'];
        $this->address2 = @$request['address2'];
        $this->email = $request['email'];
        $this->date_of_birth = $request['date_of_birth'];
        $this->city = $request['city'];
        $this->state = $request['state'];
        $this->country = $request['country'];
        $this->zip = @$request['zip'];
        $this->user_id = auth()->id();
        return $this->save();
    }

    public function user()
    {
        # code...
        return $this->belongsTo('\App\User');
    }

    public function createNew($id,$biz_name)
    {
        # code...
        $this->user_id = $id;
        $this->business_name =  str_replace(' ', '_',$biz_name);

        return $this->save();
    }
}
