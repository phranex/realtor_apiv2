<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    //
    public function createOrEdit($review, $reply)
    {
        # code...
        $this->user_id = auth()->id();
        $this->review_id = $review;
        $this->reply = $reply;
        $this->save();
    }

    public function review()
    {
        return $this->belongsTo('App\Review');
    }
}
