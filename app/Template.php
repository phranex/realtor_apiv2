<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    //

    public function createOrEdit($name,$message)
    {
        # code...
        $this->name = $name;
        $this->message = $message;
        $this->user_id = auth()->id();
        $this->save();
    }
}
