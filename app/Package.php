<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    //
    public function createOrEdit($name,$price)
    {
        # code...
        $this->name = $name;
        $this->price = $price;
        $this->save();
        return $this;
    }


    public function remove()
    {
        # code...
        \App\Quality::where('package_id', $this->id)->delete();

        $this->delete();

    }

    public function removeQualities()
    {
        # code...
        \App\Quality::where('package_id', $this->id)->delete();

    }

    public function qualities()
    {
        return $this->hasMany('App\Quality', 'package_id');
    }




    public function features()
    {
        return $this->hasManyThrough('App\Feature', 'App\Quality', 'package_id', 'id', 'id','feature_id');
    }

    public function payments()
    {
        return $this->hasManyThrough('App\Payment', 'App\Subscription', 'name', 'subscription_id', 'name','id');
    }

    public function getThisPackageTotalRevenue()
    {
        # code...
        return $this->payments->pluck('price')->sum();
    }

    public static function paymentStat()
    {
        # code...
        $plans = self::all();
        $plans_stat = [];
        if(!empty($plans)){
            foreach($plans as $plan){
                $plans_stat[$plan->name] = $plan->getThisPackageTotalRevenue();
            }
        }

        return $plans_stat;
    }
}
