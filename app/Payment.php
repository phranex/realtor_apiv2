<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Payment extends Model
{
    //
    public function createPayment($sub_id, $price)
    {
        # code...
        $this->subscription_id = $sub_id;
        $this->price = $price;
        $this->user_id = auth()->id();
        return $this->save();
    }


    public static function getTotalRevenue()
    {
        # code...
        return self::all()->pluck('price')->sum();
    }

    public static function getIncomeStat($num)
    {
        # code...
        $num_of_previous_months = $num;
        $now = \Carbon\Carbon::now();
        $months = [];
        for($i = $num_of_previous_months; $i >= 0; $i--){
            $month = \Carbon\Carbon::now()->subMonths($i)->format('M Y');
            array_push($months, $month);
        }

        $from = \Carbon\Carbon::now()->subMonths($num_of_previous_months)->startOfMonth();
        $paymentStats = self::whereBetween(DB::raw('DATE(created_at)'), array($from, $now))
                        ->orderBy('created_at','asc')
                        ->selectRaw('COUNT(DATE_FORMAT(created_at, "%b")) as count, SUM(price) as total_amt, DATE_FORMAT(created_at, "%b %Y") AS month_year')
                        ->groupBy('created_at')
                        ->get()
                        ->toArray();
        $x = array_column((array) $paymentStats, 'month_year');
        $y =  $paymentStats;


        for($i = 0; $i < count($months); $i++){

            if(array_search($months[$i], $x) !== false){
                $index = array_search($months[$i], $x);
                $income_stat[$months[$i]] = $y[$index]['total_amt'];
            }else{
                $income_stat[$months[$i]] = 0;
            }
        }

        return $income_stat;
    }

    // public static function getIncomePlanStat()
    // {
    //     # code...
    //     return $stat = self::orderBy('created_at','asc')
    //                 ->selectRaw('payments.id as id, payments.subscription_id as subscription_id, SUM(price) as total_amt')
    //                 ->groupBy('subscription_id')
    //                 ->get()
    //                 ->toArray();
    // }
}
