<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
use Laravel\Cashier\Billable;
use Stripe\Customer as StripeCustomer;



class User extends Authenticatable implements JWTSubject
{
    use Notifiable, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

     /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    public function invitations()
    {
        return $this->hasMany('App\Invitation');
    }
    public function contacts()
    {
        return $this->hasMany('App\Contact');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review');
    }

     public function thirdPartyIntegrations()
    {
        return $this->hasMany('App\Integration','user_id');
    }

    public function templates()
    {
        return $this->hasMany('App\Template','user_id');
    }



    public function setting()
    {
        return $this->hasOne('App\PersonalSetting');
    }
    public function avatar()
    {
        return $this->hasOne('App\Avatar', 'user_id');
    }

    public function socialLinks()
    {
        return $this->hasMany('App\SocialLink');
    }

    public function getUserAverageReview()
    {
        # code...
        $num_of_reviews = count($this->reviews);
        $total_rating_score = (int) 0;
        if($num_of_reviews){
            $total_rating_score = 0;
            for($i = 0; $i < $num_of_reviews; $i++){
                $total_rating_score += $this->reviews[$i]->rating_score;
            }
            $total_rating_score = $total_rating_score / $num_of_reviews;
        }
        return $total_rating_score;
    }

    public function getUserStatistics(Type $var = null)
    {
        # code...
        $contacts = [
            'total' => count($this->contacts),
        ];
        $reviews = [
            'total' => count($this->reviews)
        ];
        //get reviews where date is between now and last six month. group by month.
        $now = \Carbon\Carbon::now();
        $months = [];
        for($i = 5; $i >= 0; $i--){
            $month = \Carbon\Carbon::now()->subMonths($i)->format('M Y');
            array_push($months, $month);
        }
        $from = \Carbon\Carbon::now()->subMonths(5);
        $reviews_count = Review::where('user_id', $this->id)
                                ->whereBetween(DB::raw('DATE(created_at)'), array($from, $now))
                                ->orderBy('created_at','asc')
                                ->selectRaw('COUNT(DATE_FORMAT(created_at, "%b")) as count, DATE_FORMAT(created_at, "%b %Y") AS month_year')
                                ->groupBy('created_at')
                                ->get()
                                ->toArray();

        $x = array_column((array) $reviews_count, 'month_year');
        $y =  $reviews_count;
        for($i = 0; $i < count($months); $i++){
            $index = array_search($months[$i], $x);
            logger($index);
            if($index !== false){
                $m[$months[$i]] = $y[$index]['count'];
            }else{
                $m[$months[$i]] = 0;
            }
        }
        $result = [
            'contacts' => $contacts,
            'reviews' => $reviews,
            'avg_rating' => $this->getUserAverageReview(),
            'reviews_count' =>$m,


        ];

        return $result;
    }


    public function defaultPaymentMethod()
    {
        if (! $this->hasStripeId()) {
            return;
        }

        $customer = StripeCustomer::retrieve([
            'id' => $this->stripe_id,
            'expand' => [
                'invoice_settings.default_payment_method',
                'default_source',
            ],
        ], $this->stripeOptions());

        if ($customer->invoice_settings->default_payment_method) {
            return $customer->invoice_settings->default_payment_method;
            // return new PaymentMethod($this, $customer->invoice_settings->default_payment_method);
        }

        // If we can't find a payment method, try to return a legacy source...
        return $customer->default_source;
    }

    public function paymentSubscription()
    {
        # code...
        return $subscription = Subscription::where('user_id', $this->id)->latest()->first();

    }

}
