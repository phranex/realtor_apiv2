<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('invitation_id')->nullable();
            $table->string('title');
            $table->text('review');
            $table->string('email')->nullable();
            $table->string('fullname')->nullable();
            $table->integer('rating_score');
            $table->integer('contact_id')->nullable();
            $table->timestamps();

            $table->unique(['invitation_id', 'user_id']);
            $table->unique(['email', 'user_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
